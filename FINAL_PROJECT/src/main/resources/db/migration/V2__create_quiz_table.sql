CREATE TABLE quiz (
    id                SERIAL PRIMARY KEY,
    user_id           INT NOT NULL REFERENCES "user" (id),
    name              VARCHAR NOT NULL
);
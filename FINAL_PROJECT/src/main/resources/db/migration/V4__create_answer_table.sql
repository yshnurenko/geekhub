CREATE TABLE answer (
    id                   SERIAL PRIMARY KEY,
    question_id          INT NOT NULL REFERENCES question (id),
    body                 TEXT NOT NULL,
    correct              BOOLEAN NOT NULL
);
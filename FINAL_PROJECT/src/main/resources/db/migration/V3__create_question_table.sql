CREATE TABLE question (
    id                   SERIAL PRIMARY KEY,
    test_id              INT NOT NULL REFERENCES quiz (id),
    body                 TEXT NOT NULL
);
package org.geekhub.answer;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class AnswerRowMapper implements RowMapper<Answer> {

    @Override
    public Answer mapRow(ResultSet rs, int rowNum) throws SQLException {

        Answer answer = new Answer();
        answer.setId(rs.getInt("id"));
        answer.setQuestionId(rs.getInt("questionId"));
        answer.setCorrect(rs.getBoolean("correct"));
        answer.setBody(rs.getString("body"));

        return answer;
    }
}

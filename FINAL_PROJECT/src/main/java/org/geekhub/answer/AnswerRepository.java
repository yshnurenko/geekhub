package org.geekhub.answer;

import org.geekhub.question.Question;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AnswerRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final AnswerRowMapper answerRowMapper;

    public AnswerRepository(NamedParameterJdbcTemplate jdbcTemplate, AnswerRowMapper answerRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.answerRowMapper = answerRowMapper;
    }

    public Answer saveAnswer(Answer answer) {
        String sql = "INSERT INTO answer (question_id, correct, body) VALUES (:question_id, :correct, :body) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("question_id", answer.getQuestionId());
        params.addValue("correct", answer.isCorrect());
        params.addValue("body", answer.getBody());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        answer.setId(generatedId);

        return answer;
    }

    public List<Answer> getAllAnswers() {
        String sql = "SELECT id, question_id, correct, body FROM answer ORDER BY id ASC";
        return jdbcTemplate.query(sql, answerRowMapper);
    }

    public List<Answer> getAnswersByIds(List<Integer> answerIds) {
        String sql = "SELECT id, question_id, correct, body FROM answer WHERE id IN (:answerIds)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("answerIds", answerIds);

        return jdbcTemplate.query(sql, params, answerRowMapper);
    }

    public Answer getAnswerById(int answerId) {
        String sql = "SELECT id, question_id, correct, body FROM answer WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", answerId);

        return jdbcTemplate.queryForObject(sql, params, answerRowMapper);
    }
}

package org.geekhub.user;

import org.geekhub.quiz.Quiz;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserRowMapper userRowMapper;

    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate, UserRowMapper userRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userRowMapper = userRowMapper;
    }

    public User saveUser(User user) {
        String sql = "INSERT INTO user (name, email, password) VALUES (:name, :email, :password) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", user.getName());
        params.addValue("email", user.getEmail());
        params.addValue("password", user.getPassword());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        user.setId(generatedId);

        return user;
    }

    public List<User> getAllUsers() {
        String sql = "SELECT id, name, email, password FROM user ORDER BY id ASC";
        return jdbcTemplate.query(sql, userRowMapper);
    }

    public List<User> getUsersByIds(List<Integer> userIds) {
        String sql = "SELECT id, name, email, password FROM user WHERE id IN (:quizIds)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userIds", userIds);

        return jdbcTemplate.query(sql, params, userRowMapper);
    }

    public User getUserById(int userId) {
        String sql = "SELECT id, name, email, password FROM user WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", userId);

        return jdbcTemplate.queryForObject(sql, params, userRowMapper);
    }
}

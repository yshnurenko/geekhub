package org.geekhub.quiz;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/quizzes")
public class QuizController {

    private final QuizService quizService;

    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        List<Quiz> quizzes = quizService.getAllQuizzes();
        model.addAttribute("quizzes", quizzes);

        return "quiz/index";
    }

    @GetMapping("/create")
    public String getCreatePage() {
        return "quiz/create";
    }

    @PostMapping("/create")
    public String createAuthor(@RequestParam String name,
                               @RequestParam Integer userId) {

        Quiz quiz = quizService.createQuiz(userId, name);
        return "redirect:/quizzes/" + quiz.getId() + "/show";
    }

    @GetMapping("/{quizId}/show")
    public String getShowAuthorPage(@PathVariable int quizId, ModelMap model) {
        Quiz quiz = quizService.getQuizById(quizId);
        model.addAttribute("quiz", quiz);
        return "quiz/show";
    }
}

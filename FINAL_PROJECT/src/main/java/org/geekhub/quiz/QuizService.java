package org.geekhub.quiz;

import org.geekhub.answer.AnswerRepository;
import org.geekhub.question.Question;
import org.geekhub.question.QuestionRepository;
import org.geekhub.user.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizService {

    private final UserRepository userRepository;
    private final QuizRepository quizRepository;
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;

    public QuizService(UserRepository userRepository, QuizRepository quizRepository, QuestionRepository questionRepository, AnswerRepository answerRepository) {
        this.userRepository = userRepository;
        this.quizRepository = quizRepository;
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
    }

    public Quiz createQuiz(Integer userId, String name) {
        Quiz quiz = new Quiz();
        quiz.setUserId(userId);
        quiz.setName(name);

        return quizRepository.saveQuiz(quiz);
    }

    public Question createQuestion(Integer quizId, String body) {
        Question question = new Question();
        question.setQuiz_id(quizId);
        question.setBody(body);

        return questionRepository.saveQuestion(question);
    }

    public Quiz getQuizById(Integer quizId) {
        return quizRepository.getQuizById(quizId);
    }

    public List<Quiz> getAllQuizzes() {
        return quizRepository.getAllQuizzes();
    }
}

package org.geekhub.quiz;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuizRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final QuizRowMapper quizRowMapper;

    public QuizRepository(NamedParameterJdbcTemplate jdbcTemplate, QuizRowMapper quizRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.quizRowMapper = quizRowMapper;
    }

    public Quiz saveQuiz(Quiz quiz) {
        String sql = "INSERT INTO quiz (user_id, name) VALUES (:user_id, :name) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id", quiz.getUserId());
        params.addValue("name", quiz.getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        quiz.setId(generatedId);

        return quiz;
    }

    public List<Quiz> getAllQuizzes() {
        String sql = "SELECT id, user_id, name FROM quiz ORDER BY id ASC";
        return jdbcTemplate.query(sql, quizRowMapper);
    }

    public List<Quiz> getQuizzesByIds(List<Integer> quizIds) {
        String sql = "SELECT id, user_id, name FROM quiz WHERE id IN (:quizIds)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("quizIds", quizIds);

        return jdbcTemplate.query(sql, params, quizRowMapper);
    }

    public Quiz getQuizById(int quizId) {
        String sql = "SELECT id, user_id, name FROM quiz WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", quizId);

        return jdbcTemplate.queryForObject(sql, params, quizRowMapper);
    }
}

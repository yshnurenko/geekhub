package org.geekhub.quiz;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class QuizRowMapper implements RowMapper<Quiz> {

    @Override
    public Quiz mapRow(ResultSet rs, int rowNum) throws SQLException {

        Quiz quiz = new Quiz();
        quiz.setId(rs.getInt("id"));
        quiz.setUserId(rs.getInt("user_id"));
        quiz.setName(rs.getString("name"));

        return quiz;
    }
}

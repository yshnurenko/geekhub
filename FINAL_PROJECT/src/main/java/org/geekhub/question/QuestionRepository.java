package org.geekhub.question;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final QuestionRowMapper questionRowMapper;

    public QuestionRepository(NamedParameterJdbcTemplate jdbcTemplate, QuestionRowMapper questionRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.questionRowMapper = questionRowMapper;
    }

    public Question saveQuestion(Question question) {
        String sql = "INSERT INTO question (quiz_id, body) VALUES (:quiz_id, :body) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("quiz_id", question.getQuiz_id());
        params.addValue("body", question.getBody());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        question.setId(generatedId);

        return question;
    }

    public List<Question> getAllQuestions() {
        String sql = "SELECT id, quiz_id, body FROM question ORDER BY id ASC";
        return jdbcTemplate.query(sql, questionRowMapper);
    }

    public List<Question> getQuestionsByIds(List<Integer> questionIds) {
        String sql = "SELECT id, quiz_id, body FROM question WHERE id IN (:questionIds)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("questionIds", questionIds);

        return jdbcTemplate.query(sql, params, questionRowMapper);
    }

    public Question getQuestionById(int questionId) {
        String sql = "SELECT id, user_id, name FROM quiz WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", questionId);

        return jdbcTemplate.queryForObject(sql, params, questionRowMapper);
    }
}

package org.geekhub.task2;

class ArraySorter {

    static <T extends Comparable<T>> T[] bubbleSort(T[] array, Direction direction) {
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                T element1 = array[j];
                T element2 = array[j + 1];
                int condition = direction.getDirection();
                if (element1.compareTo(element2) > 0 && condition > 0 ||
                        element1.compareTo(element2) < 0 && condition < 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }

    static <T extends Comparable<T>> T[] insertionSort(T[] array, Direction direction) {
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j > 0; j--) {
                T element1 = array[j];
                T element2 = array[j - 1];
                int condition = direction.getDirection();
                if (element1.compareTo(element2) > 0 && condition < 0 ||
                        element1.compareTo(element2) < 0 && condition > 0) {
                    T temp = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = temp;
                }
            }
        }
        return array;
    }

    static <T extends Comparable<T>> T[] selectionSort(T[] array, Direction direction) {
        for (int i = 0; i < array.length; i++) {
            int minI = i;
            for (int j = i + 1; j < array.length; j++) {
                T element1 = array[i];
                T element2 = array[j];
                int condition = direction.getDirection();
                if (element1.compareTo(element2) > 0 && condition > 0 ||
                        element1.compareTo(element2) < 0 && condition < 0) {
                    minI = j;
                }
            }
            if (i != minI) {
                T temp = array[i];
                array[i] = array[minI];
                array[minI] = temp;
            }
        }
        return array;
    }
}


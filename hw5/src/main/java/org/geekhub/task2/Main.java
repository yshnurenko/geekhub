package org.geekhub.task2;

public class Main {
    public static void main(String[] args) {
        User u = new User("Alex", 10);
        User u1 = new User("John", 14);
        User u2 = new User("Antony", 11);
        User u3 = new User("Robert", 22);
        User u4 = new User("Ann", 98);
        User u5 = new User("Oliver", 89);
        User u6 = new User("Mike", 32);
        User u7 = new User("Tommy", 43);
        User u8 = new User("Any", 51);
        User u9 = new User("George", 41);
        User u10 = new User("Bran", 19);

        User[] users = {u, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10};

        User[] bubbleSortASC = ArraySorter.bubbleSort(users, Direction.ASC);
        System.out.println("BubbleSort ASC: ");
        for (User user : bubbleSortASC) {
            System.out.println(user);
        }

        User[] bubbleSortDESC = ArraySorter.bubbleSort(users, Direction.DESC);
        System.out.println("BubbleSort DESC: ");
        for (User user : bubbleSortDESC) {
            System.out.println(user);
        }

        User[] insertSortASC = ArraySorter.insertionSort(users, Direction.ASC);
        System.out.println("InsertSort ASC: ");
        for (User user : insertSortASC) {
            System.out.println(user);
        }

        User[] insertSortDESC = ArraySorter.insertionSort(users, Direction.DESC);
        System.out.println("InsertSort DESC: ");
        for (User user : insertSortDESC) {
            System.out.println(user);
        }

        User[] selectSortASC = ArraySorter.selectionSort(users, Direction.ASC);
        System.out.println("SelectSort ASC: ");
        for (User user : selectSortASC) {
            System.out.println(user);
        }

        User[] selectSortDESC = ArraySorter.selectionSort(users, Direction.DESC);
        System.out.println("SelectSort DESC: ");
        for (User user : selectSortDESC) {
            System.out.println(user);
        }
    }
}

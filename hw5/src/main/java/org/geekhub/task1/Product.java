package org.geekhub.task1;

public abstract class Product {

    private String name;
    private double price;
    private int count;

    Product(String name, double price, int count) {
        this.name = name;
        this.price = price;
        this.count = count;
    }

    double getPriceProduct() {
        return price * count;
    }

    @Override
    public String toString() {
        return "{" + name + '\'' +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}

package org.geekhub.task1;

public enum ProductType {
    FRUIT,
    VEGETABLES,
    MEAT
}

package org.geekhub.task1;

class Fruit extends Product {

    Fruit(String name, double price, int count) {
        super(name, price, count);
    }
}

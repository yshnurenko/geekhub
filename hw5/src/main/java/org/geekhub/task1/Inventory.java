package org.geekhub.task1;

import java.util.ArrayList;
import java.util.List;

class Inventory {

    private List<Product> products = new ArrayList<>();

    void addProduct(Product product) {
        products.add(product);
    }

    double amountProduct() {
        return products.stream()
                .mapToDouble(Product::getPriceProduct)
                .sum();
    }
}


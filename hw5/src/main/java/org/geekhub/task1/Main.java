package org.geekhub.task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Inventory inventory = new Inventory();

        do {
            System.out.println("Enter the type product(FRUIT,VEGETABLES,MEAT): ");
            String input = scanner.nextLine();
            System.out.println("Enter the name, price, count: ");
            String productInfo = scanner.nextLine();
            String[] arrayProductInfo = productInfo.split(",");
            String name = (arrayProductInfo[0]);
            if (name.equals("exit")) {
                return;
            }
            ProductType productType = ProductType.valueOf(input);
            double price = Double.parseDouble(arrayProductInfo[1]);
            int count = Integer.parseInt(arrayProductInfo[2]);

            switch (productType) {
                case FRUIT:
                    Product fruit = new Fruit(name, price, count);
                    inventory.addProduct(fruit);
                    break;
                case VEGETABLES:
                    Product vegetables = new Vegetables(name, price, count);
                    inventory.addProduct(vegetables);
                    break;
                case MEAT:
                    Product meat = new Meat(name, price, count);
                    inventory.addProduct(meat);
                    break;
                default:
            }
            System.out.println(inventory.amountProduct());
        } while (true);
    }
}

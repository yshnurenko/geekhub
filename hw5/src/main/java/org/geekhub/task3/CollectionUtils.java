package org.geekhub.task3;

import java.util.*;
import java.util.function.*;

class CollectionUtils {

    private CollectionUtils() {
        throw new UnsupportedOperationException();
    }

    static <E> List<E> generate(Supplier<E> generator,
                                Supplier<List<E>> listFactory,
                                int count) {
        List<E> list = listFactory.get();
        for (int i = 0; i < count; i++) {
            list.add(generator.get());
        }
        return list;
    }

    static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> list = new ArrayList<>();
        for (E element : elements) {
            if (filter.test(element)) {
                list.add(element);
            }
        }
        return list;
    }

    static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E elem : elements) {
            if (predicate.test(elem)) {
                return true;
            }
        }
        return false;
    }

    static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E elem : elements) {
            if (!predicate.test(elem)) {
                return false;
            }
        }
        return true;
    }

    static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E elem : elements) {
            if (predicate.test(elem)) {
                return false;
            }
        }
        return true;
    }

    static <T, R> List<R> map(List<T> elements,
                              Function<T, R> mappingFunction,
                              Supplier<List<R>> listFactory) {
        List<R> list = listFactory.get();
        for (T elem : elements) {
            list.add(mappingFunction.apply(elem));
        }
        return list;
    }

    static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        List<E> list = new ArrayList<>(elements);
        list.sort(comparator);
        return Optional.of(list.get(list.size() - 1));
    }

    static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        List<E> list = new ArrayList<>(elements);
        list.sort(comparator);
        return Optional.of(list.get(0));
    }

    static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> list = listFactory.get();
        Set<E> set = new HashSet<>(elements);
        list.addAll(set);
        return list;
    }

    static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E elem : elements) {
            consumer.accept(elem);
        }
    }

    static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        E e = elements.get(0);
        for (int i = 1; i < elements.size(); i++) {
            e = accumulator.apply(e, elements.get(i));
        }
        return Optional.of(e);
    }

    static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E e = seed;
        for (E elem : elements) {
            e = accumulator.apply(e, elem);
        }
        return e;
    }

    static <E> Map<Boolean, List<E>> partitionBy(List<E> elements,
                                                 Predicate<E> predicate,
                                                 Supplier<Map<Boolean, List<E>>> mapFactory,
                                                 Supplier<List<E>> listFactory) {
        Map<Boolean, List<E>> map = mapFactory.get();
        map.put(Boolean.TRUE, listFactory.get());
        map.put(Boolean.FALSE, listFactory.get());
        for (E elem : elements) {
            map.get(predicate.test(elem)).add(elem);
        }
        return map;
    }

    static <T, K> Map<K, List<T>> groupBy(List<T> elements,
                                          Function<T, K> classifier,
                                          Supplier<Map<K, List<T>>> mapFactory,
                                          Supplier<List<T>> listFactory) {
        Map<K, List<T>> map = mapFactory.get();
        for (T elem : elements) {
            K key = classifier.apply(elem);
            if (!map.containsKey(key)) {
                map.put(key, listFactory.get());
            }
            map.get(key).add(elem);
        }
        return map;
    }

    static <T, K, U> Map<K, U> toMap(List<T> elements,
                                     Function<T, K> keyFunction,
                                     Function<T, U> valueFunction,
                                     BinaryOperator<U> mergeFunction,
                                     Supplier<Map<K, U>> mapFactory) {
        Map<K, U> map = mapFactory.get();
        for (T elem : elements) {
            K key = keyFunction.apply(elem);
            if (map.containsKey(key)) {
                map.put(key, mergeFunction.apply(map.get(key), valueFunction.apply(elem)));
            } else {
                map.put(key, valueFunction.apply(elem));
            }
        }
        return map;
    }

    static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements,
                                                                 Predicate<E> predicate,
                                                                 Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                 Supplier<List<T>> listFactory,
                                                                 Function<E, T> elementMapper) {
        Map<Boolean, List<T>> result = mapFactory.get();
        for (E element : elements) {
            boolean key = predicate.test(element);
            List<T> value = result.getOrDefault(key, listFactory.get());

            value.add(elementMapper.apply(element));

            result.put(key, value);

        }
        return result;
    }

    static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements,
                                                          Function<T, K> classifier,
                                                          Supplier<Map<K, List<U>>> mapFactory,
                                                          Supplier<List<U>> listFactory,
                                                          Function<T, U> elementMapper) {

        Map<K, List<U>> result = mapFactory.get();
        for (T element : elements) {
            K key = classifier.apply(element);
            List<U> value = result.getOrDefault(key, listFactory.get());

            value.add(elementMapper.apply(element));

            result.put(key, value);

        }
        return result;
    }
}
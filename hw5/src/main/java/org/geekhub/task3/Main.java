package org.geekhub.task3;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 2, 2, 3, 3, 3, 4, 5, 6, 7, 7, 8, 9, 10, 11, 12);

        System.out.println("Generate: ");
        System.out.println(CollectionUtils.generate(() -> 4, ArrayList::new, 2));

        System.out.println("Filter: ");
        System.out.println(CollectionUtils.filter(numbers, a -> a > 10));

        System.out.println("AllMatch, AnyMatch, NoneMatch: ");
        System.out.println(CollectionUtils.allMatch(numbers, a -> a > 5));
        System.out.println(CollectionUtils.anyMatch(numbers, a -> a > 5));
        System.out.println(CollectionUtils.noneMatch(numbers, a -> a > 5));

        System.out.println("Map: ");
        System.out.println(CollectionUtils.map(numbers, Integer::doubleValue, ArrayList::new));

        System.out.println("Max, Min: ");
        System.out.println(CollectionUtils.max(numbers, Comparator.comparingInt(a -> a)));
        System.out.println(CollectionUtils.min(numbers, Comparator.comparingInt(a -> a)));

        System.out.println("Distinct: ");
        System.out.println(CollectionUtils.distinct(numbers, ArrayList::new));

        System.out.println("ForEach: ");
        CollectionUtils.forEach(numbers, System.out::println);

        System.out.println("Reduce: ");
        System.out.println(CollectionUtils.reduce(numbers, Integer::sum));
        System.out.println(CollectionUtils.reduce(6, numbers, Integer::sum));

        System.out.println("PartitionBy: ");
        System.out.println(CollectionUtils.partitionBy(numbers, a -> a > 5, HashMap::new, ArrayList::new));

        System.out.println("GroupBy: ");
        System.out.println(CollectionUtils.groupBy(numbers, a -> a, HashMap::new, ArrayList::new));

        System.out.println("ToMap: ");
        System.out.println(CollectionUtils.toMap(numbers, key -> key, value -> value, Integer::sum, HashMap::new));

        System.out.println("PartitionByAndMapElement: ");
        System.out.println(CollectionUtils.partitionByAndMapElement(numbers, a -> a > 5, HashMap::new, ArrayList::new, a -> a));

        System.out.println("GroupByAndMapElement: ");
        System.out.println(CollectionUtils.groupByAndMapElement(numbers, a -> a, HashMap::new, ArrayList::new, a -> a));
    }
}
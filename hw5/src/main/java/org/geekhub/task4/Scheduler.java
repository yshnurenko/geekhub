package org.geekhub.task4;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

class Scheduler {

    static List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter(task -> task.getType() == TaskType.IMPORTANT)
                .sorted(Comparator.comparing(Task::getStartsOn))
                .limit(5)
                .collect(toList());
    }

    static List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .flatMap(task -> task.getCategories()
                        .stream())
                .distinct()
                .collect(toList());
    }

    static Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
                .flatMap(t -> t.getCategories()
                        .stream()
                        .map(c -> new Pair<>(c, t)))
                .collect(Collectors.groupingBy(p -> p.m, Collectors.mapping(p -> p.n, toList())));
    }

    static Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(Task::isDone));
    }

    static boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream()
                .anyMatch(t -> t.getCategories().contains(category));
    }

    static String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .skip(startNo)
                .limit(endNo - startNo)
                .map(Task::getTitle)
                .collect(Collectors.joining(","));
    }

    static Map<String, Long> getCountsByCategories(List<Task> tasks) {
        Map<String, List<Pair<Task, String>>> map = tasks.stream()
                .flatMap(t -> t.getCategories()
                        .stream()
                        .map(c -> new Pair<>(c, t)))
                .collect(Collectors.groupingBy(p -> p.m));
        Map<String, Long> result = new HashMap<>();
        for (Map.Entry<String, List<Pair<Task, String>>> entry : map.entrySet()) {
            result.put(entry.getKey(), (long) entry.getValue().size());
        }
        return result;
    }

    static IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.summarizingInt(Task::hashCode));
    }

    static Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
                .max(Comparator.comparingInt(task -> task.getCategories().size()))
                .orElse(null);
    }

    private static class Pair<N, M> {
        private M m;
        private N n;

        private Pair(M m, N n) {
            this.m = m;
            this.n = n;
        }
    }
}
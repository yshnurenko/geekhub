package org.geekhub.task4;

import java.time.LocalDate;
import java.util.Set;


public class Task {
    private int id;
    private TaskType type;
    private String title;
    private boolean done;
    private Set<String> categories;
    private LocalDate startsOn;

    Task(int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories = categories;
        this.startsOn = startsOn;
    }

    int getId() {
        return id;
    }

    TaskType getType() {
        return type;
    }

    String getTitle() {
        return title;
    }

    boolean isDone() {
        return done;
    }

    Set<String> getCategories() {
        return categories;
    }

    LocalDate getStartsOn() {
        return startsOn;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", done=" + done +
                ", categories=" + categories +
                ", startsOn=" + startsOn +
                '}';
    }
}
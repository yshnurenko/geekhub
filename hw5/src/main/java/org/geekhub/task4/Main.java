package org.geekhub.task4;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> categories = new HashSet<>();
        categories.add("HomeWork");
        categories.add("ClassWork");
        categories.add("OfficeWork");

        List<Task> tasks = new ArrayList<>();
        Task task1 = new Task(1, TaskType.IMPORTANT, "hw1", false, categories, LocalDate.now());
        Task task2 = new Task(2, TaskType.UNIMPORTANT, "cw2", true, categories, LocalDate.now());
        Task task3 = new Task(3, TaskType.IMPORTANT, "hw3", true, categories, LocalDate.now());
        Task task4 = new Task(3, TaskType.IMPORTANT, "hw4", true, categories, LocalDate.now());
        Task task5 = new Task(3, TaskType.IMPORTANT, "ow1", true, categories, LocalDate.now());
        Task task6 = new Task(3, TaskType.IMPORTANT, "hw2", true, categories, LocalDate.now());
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);
        tasks.add(task5);
        tasks.add(task6);

        System.out.println("Find5NearestImportantTasks: ");
        System.out.println(Scheduler.find5NearestImportantTasks(tasks));

        System.out.println("GetUniqueCategories: ");
        System.out.println(Scheduler.getUniqueCategories(tasks));

        System.out.println("GetCategoriesWithTasks: ");
        System.out.println(Scheduler.getCategoriesWithTasks(tasks));

        System.out.println("SplitTasksIntoDoneAndInProgress: ");
        System.out.println(Scheduler.splitTasksIntoDoneAndInProgress(tasks));

        System.out.println("ExistsTaskOfCategory: ");
        System.out.println(Scheduler.existsTaskOfCategory(tasks, "HomeWork"));

        System.out.println("GetTitlesOfTasks: ");
        System.out.println(Scheduler.getTitlesOfTasks(tasks, 0, 2));

        System.out.println("GetCountsByCategories: ");
        System.out.println(Scheduler.getCountsByCategories(tasks));

        System.out.println("GetCategoriesNamesLengthStatistics: ");
        System.out.println(Scheduler.getCategoriesNamesLengthStatistics(tasks));

        System.out.println("findTaskWithBiggestCountOfCategories: ");
        System.out.println(Scheduler.findTaskWithBiggestCountOfCategories(tasks));
    }
}
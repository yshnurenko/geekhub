package org.geekhub.task2;

import java.io.*;
import java.net.URL;

/**
 * Represents worker that downloads image from URL to specified folder.<br/>
 * Name of the image will be constructed based on URL. Names for the same URL will be the same.
 */
public class ImageTask implements Runnable {

    private final URL url;
    private final String folder;

    /**
     * Inherited method that do main job - downloads the image and stores it at specified location
     */
    public ImageTask(URL url, String folder) {
        this.url = url;
        this.folder = folder;
    }

    /**
     * Inherited method that do main job - downloads the image and stores it at specified location
     */
    @Override
    public void run() {
        String fileName = buildFileName(url);
        try (InputStream in = new BufferedInputStream(url.openStream());
             ByteArrayOutputStream out = new ByteArrayOutputStream();
             FileOutputStream fos = new FileOutputStream(folder + fileName)) {

            byte[] buf = new byte[2048];
            int n;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            byte[] response = out.toByteArray();
            fos.write(response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String buildFileName(URL url) {
        return url.toString().replaceAll("[^a-zA-Z0-9-_.]", "_");
    }
}

package org.geekhub.task2;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that represents page that could be downloaded by specified URL.
 * Allows high-level access to page elements.
 */
public class Page {

    private Pattern imageLinkPattern = Pattern.compile("<img.*?src=\"(.*?)\".*?(/>|</img>)");

    private final URL url;
    private final String content;

    /**
     * Be careful, constructor downloads content, it could be slow.
     *
     * @param url
     * @throws IOException
     */
    public Page(URL url) throws IOException {
        this.url = url;
        this.content = new String(ConnectionUtils.getData(url));
    }

    /**
     * Extracts all links to images from the page like <img src={link}/>. Method does not cache content. Each time new list will be returned.
     *
     * @return list of URLs to images from that page.
     * @throws MalformedURLException
     */
    public Collection<URL> getImageLinks() throws MalformedURLException {
        Matcher matcher = imageLinkPattern.matcher(content);
        return extractMatches(matcher);
    }

    private Collection<URL> extractMatches(Matcher matcher) throws MalformedURLException {
        List<String> list = new ArrayList<>();
        while (matcher.find()) {
            list.add(matcher.group(1));
        }

        List<URL> result = new ArrayList<>();
        for (String s : list) {
            URL url = new URL(s);
            result.add(url);
        }
        return result;
    }
}

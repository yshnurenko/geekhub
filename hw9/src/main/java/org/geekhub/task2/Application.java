package org.geekhub.task2;

import java.io.IOException;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) throws IOException {
        ImageCrawler imageCrawler = new ImageCrawler(4, "hw9/src/main/java/files/task2/");
        imageCrawler.downloadImages("https://bipbap.ru/krasivye-kartinki/skachat-krasivye-kartinki-na-telefon-besplatno-38-foto.html");

        System.out.println("While it's loading you can enter another url to start download images:");

        Scanner scanner = new Scanner(System.in);
        String command;
        while (!"exit".equals(command = scanner.next())) {
            imageCrawler.downloadImages(command);
            System.out.println("...and another url:");
        }
        imageCrawler.stop();
    }
}

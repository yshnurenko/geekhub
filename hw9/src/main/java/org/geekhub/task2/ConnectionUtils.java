package org.geekhub.task2;

import java.io.*;
import java.net.URL;

public class ConnectionUtils {

    private ConnectionUtils() {
    }

    public static byte[] getData(URL url) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (InputStream is = url.openStream()) {
            byte[] data = new byte[4096];
            int n;

            while ((n = is.read(data)) > 0) {
                baos.write(data, 0, n);
            }
        }
        return baos.toByteArray();
    }
}

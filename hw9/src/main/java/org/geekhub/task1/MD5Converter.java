package org.geekhub.task1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MD5Converter {

    private final MD5HashProvider md5HashProvider;

    public MD5Converter(MD5HashProvider md5HashProvider) {
        this.md5HashProvider = md5HashProvider;
    }

    public final void convertContentToMd5(String pathToFile) throws IOException {
        List<String> contentUrls = FileWorker.readFile(pathToFile);
        List<String> md5Hashes = toMd5Hashes(contentUrls);
        FileWorker.writeFile(md5Hashes);
    }

    private List<String> toMd5Hashes(List<String> contentUrls) {
        ExecutorService taskExecutor = Executors.newFixedThreadPool(contentUrls.size());

        try {
            List<Future<String>> futures = new ArrayList<>();
            for (String contentUrl : contentUrls) {
                Future<String> future = taskExecutor.submit(() -> toMd5Hash(contentUrl));
                futures.add(future);
            }
            List<String> hashes = new ArrayList<>();
            for (Future<String> future : futures) {
                try {
                    hashes.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
            return hashes;
        } finally {
            taskExecutor.shutdown();
        }
    }

    private String toMd5Hash(String url) {
        try {
            String content = WebPageContentProvider.getContentWebPage(url);
            return md5HashProvider.getMd5(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
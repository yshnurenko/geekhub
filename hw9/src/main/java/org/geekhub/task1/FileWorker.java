package org.geekhub.task1;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileWorker {

    static List<String> readFile(String path) throws IOException {
        return Files.readAllLines(Paths.get(path, "start.txt"), Charset.defaultCharset());
    }

    static void writeFile(List<String> md5Signs) throws IOException {
        try (FileWriter writer = new FileWriter("hw9/src/main/java/files/task1/finish.txt")) {
            for (String md5Sign : md5Signs) {
                writer.write(md5Sign);
                writer.write("\n");
            }
        }
    }
}
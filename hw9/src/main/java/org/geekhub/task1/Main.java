package org.geekhub.task1;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        MD5HashProvider md5HashProvider = new MD5HashProvider();
        MD5Converter md5Converter = new MD5Converter(md5HashProvider);
        md5Converter.convertContentToMd5("hw9/src/main/java/files/task1");
    }
}
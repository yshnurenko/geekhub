package org.geekhub.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class WebPageContentProvider {

    static String getContentWebPage(String content) throws IOException {
        URL url = new URL(content);
        URLConnection con = url.openConnection();
        InputStream is = con.getInputStream();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {

            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        }
    }
}
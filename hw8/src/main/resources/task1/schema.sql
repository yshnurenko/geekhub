create table if not exists customer
(
    id serial not null
        constraint customer_pkey
            primary key,
    first_name varchar(50),
    last_name varchar(50),
    cell_phone varchar(50)
);

alter table customer owner to postgres;

create table if not exists product
(
    id serial not null
        constraint product_pkey
            primary key,
    name varchar(50),
    description varchar(50),
    current_price numeric
);

alter table product owner to postgres;

create table if not exists "order"
(
    customer_id integer
        constraint order_customer_id_fkey
            references customer,
    product_id integer
        constraint order_product_id_fkey
            references product,
    quantity integer,
    price numeric,
    delivery_place varchar(50)
);

alter table "order" owner to postgres;
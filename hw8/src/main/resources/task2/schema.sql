create table if not exists "user"
(
	id serial not null,
	name varchar,
	age integer,
	admin boolean,
	balance numeric
);

alter table "user" owner to postgres;

create table if not exists cat
(
	id serial not null,
	name varchar,
	age integer
);

alter table cat owner to postgres;
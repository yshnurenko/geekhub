CREATE TABLE IF NOT EXISTS "passengers" (
  "id"             SERIAL      NOT NULL,
  "first_name"     VARCHAR(50) NOT NULL,
  "last_name"      VARCHAR(50) NOT NULL,
  PRIMARY KEY ("id")
);

INSERT INTO "passengers" ("id", "first_name", "last_name") VALUES
  (1, 'John', 'Shnow'),
  (2, 'Nick', 'Jackson'),
  (3, 'Richard', 'Hardman'),
  (4, 'Ann', 'Algeria'),
  (5, 'Agatha', 'MacDonald'),
  (6, 'Albert', 'O�Connor'),
  (7, 'Stacey', 'Longman'),
  (8, 'Thomas', 'Little'),
  (9, 'Victor', 'Bishop'),
  (10, 'William', 'Jacobson');

CREATE TABLE IF NOT EXISTS "tickets"
(
    "id"           SERIAL  NOT NULL,
    "train_id"     INTEGER NOT NULL DEFAULT '1' REFERENCES trains (id),
    "carriage"     INTEGER NOT NULL,
    "place"        INTEGER NOT NULL,
    "price"        NUMERIC NOT NULL,
    "passenger_id" INTEGER NOT NULL DEFAULT '1' REFERENCES passengers (id),
    PRIMARY KEY ("id")
);

INSERT INTO "tickets" ("id", "train_id", "carriage", "place", "price", "passenger_id")
VALUES (1, 1, 3, 23, 300, 10),
       (2, 2, 1, 24, 550, 9),
       (3, 1, 2, 13, 300, 8),
       (4, 2, 1, 14, 550, 7),
       (5, 2, 2, 15, 550, 6),
       (6, 1, 4, 16, 300, 5),
       (7, 2, 3, 34, 550, 4),
       (8, 1, 1, 35, 300, 3),
       (9, 1, 2, 41, 300, 2),
       (10, 2, 4, 44, 550, 1);

CREATE TABLE IF NOT EXISTS "trains" (
 "id"        SERIAL      NOT NULL,
 "name"      VARCHAR(50) NOT NULL,
 PRIMARY KEY ("id")
);

INSERT INTO "trains" ("id", "name") VALUES
 (1, '734 IC+'),
 (2, '642 IC+');

CREATE TABLE IF NOT EXISTS "trips"
(
    "id"             SERIAL      NOT NULL,
    "train_id"       INTEGER     NOT NULL DEFAULT '1' REFERENCES trains (id),
    "ticket_id"      INTEGER     NOT NULL DEFAULT '1' REFERENCES tickets (id),
    "whence"         VARCHAR(50) NOT NULL,
    "departure_date" DATE        NOT NULL,
    "where"          VARCHAR(50) NOT NULL,
    "date_arrival"   DATE        NOT NULL,
    PRIMARY KEY ("id")
);

INSERT INTO "trips" ("id", "train_id", "ticket_id", whence, departure_date, "where", date_arrival)
VALUES (1, 1, 1, 'Kansas', '26-12-2019', 'Mexico', '27-12-2019'),
       (2, 2, 2, 'Arizona', '23-12-2019', 'Nebraska', '24-12-2019');
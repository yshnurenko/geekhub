/*1*/
SELECT c.sortname, c.name, c.phonecode, COUNT(s.id) AS state_count
FROM countries c
         INNER JOIN states s ON c.id = s.country_id
GROUP BY c.sortname, c.name, c.phonecode
ORDER BY state_count DESC
LIMIT 1

/*2*/
SELECT *,
       (SELECT COUNT(*) FROM cities WHERE cities.state_id = c.id) AS cities_count
FROM countries c
ORDER BY cities_count DESC
LIMIT 1

/*3*/
SELECT s.country_id, c.name, COUNT(s.id) AS state_count
FROM countries c
         INNER JOIN states s ON c.id = s.country_id
GROUP BY s.country_id, c.name
ORDER BY s.country_id, c.name, state_count DESC

/*4*/
SELECT s.country_id, c.name, COUNT(ci.name) AS city_count
FROM countries c
         INNER JOIN states s ON c.id = s.country_id
         INNER JOIN cities ci ON s.id = ci.state_id
GROUP BY s.country_id, c.name
ORDER BY s.country_id, c.name, city_count DESC

/*5*/
SELECT c.name, COUNT(distinct s.name) AS state_count, COUNT(ci.name) AS city_count
FROM countries c
         INNER JOIN states s
                    ON c.id = s.country_ID
         INNER JOIN cities ci
                    ON s.id = ci.state_id
GROUP BY c.name

/*6*/
SELECT *,
       (SELECT COUNT(*) FROM cities WHERE cities.state_id = c.id) AS cities_count
FROM countries c
ORDER BY cities_count DESC
LIMIT 10

/*7*/
(SELECT c.name, COUNT(s.id) AS state_count
 FROM countries c
          INNER JOIN states s ON c.id = s.country_id
 GROUP BY c.name, s.country_id
 ORDER BY state_count DESC
 LIMIT 10)
UNION ALL
(SELECT c.name, COUNT(s.id) AS state_count
 FROM countries c
          INNER JOIN states s ON c.id = s.country_id
 GROUP BY c.name, s.country_id
 ORDER BY state_count ASC
 LIMIT 10)

/*8*/
SELECT c.name, COUNT(s.id) AS statecount
FROM countries c
         INNER JOIN states s ON c.id = s.country_id
GROUP BY c.name
HAVING COUNT(s.id) > (SELECT AVG(countriesCount)
                      FROM (SELECT COUNT(s.id) AS countriesCount
                            FROM countries c
                                     INNER JOIN states s ON c.id = s.country_id
                            GROUP BY c.name) AS MyTable)
ORDER BY statecount DESC

/*9*/
SELECT DISTINCT ON (COUNT(s.id)) c.sortname,
                                 c.name,
                                 c.phonecode,
                                 COUNT(s.id) total_states
FROM countries c
         INNER JOIN states s ON c.id = s.country_id
GROUP BY c.sortname, c.name, c.phonecode
ORDER BY total_states, c.name

/*10*/
SELECT s.name, COUNT(s.name) AS duplicate
FROM states s
GROUP BY s.name
HAVING COUNT(s.name) > 1

/*11*/
SELECT *
FROM states s
         LEFT JOIN cities ci ON s.id = ci.state_id
WHERE ci.id IS NULL
ORDER BY s.name


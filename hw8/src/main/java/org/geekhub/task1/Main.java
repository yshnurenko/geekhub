package org.geekhub.task1;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {
        TableCreator tableCreator = new TableCreator();
        tableCreator.createTable("CREATE TABLE IF NOT EXISTS customer " +
                "(id SERIAL, " +
                " first_name VARCHAR(50), " +
                " last_name VARCHAR (50), " +
                " cell_phone VARCHAR (50), " +
                " PRIMARY KEY (id))");
        System.out.println("Customer table created.");

        tableCreator.createTable("CREATE TABLE IF NOT EXISTS product " +
                "(id SERIAL, " +
                " name VARCHAR(50), " +
                " description VARCHAR (50), " +
                " current_price NUMERIC, " +
                " PRIMARY KEY (id))");
        System.out.println("Product table created.");

        tableCreator.createTable("CREATE TABLE IF NOT EXISTS public.order " +
                "(customer_id INT REFERENCES customer (id), " +
                " product_id INT REFERENCES product (id), " +
                " quantity INTEGER, " +
                " price NUMERIC, " +
                " delivery_place VARCHAR (50))");
        System.out.println("Order table created.");

        ConnectionProvider connectionProvider = new ConnectionProvider();
        TableManager tableInitializer = new TableManager(connectionProvider);
        tableInitializer.fillingTable("INSERT INTO customer" +
                " (id, first_name, last_name, cell_phone)" +
                " VALUES (1, 'John', 'Snow', '0508244541')," +
                " (2, 'Joe', 'West', '0667564541')," +
                " (3, 'Nick', 'Nilson', '0934534321')");
        System.out.println("Table customer is filled.");

        tableInitializer.fillingTable("INSERT INTO product" +
                " (id, name, description, current_price)" +
                " VALUES (1, 'Phone', 'Iphone', 15000)," +
                " (2, 'Notebook', 'Aser', 10000)," +
                " (3, 'Phone', 'Xiaomi', 8000)");
        System.out.println("Table product is filled.");

        tableInitializer.fillingTable("INSERT INTO public.order" +
                " (customer_id, product_id, quantity, price, delivery_place)" +
                " VALUES (1, 2, 2, 30000, 'Donetsk')," +
                " (2, 1, 4, 40000, 'Kiev')," +
                " (3, 3, 7, 56000, 'Lviv')");
        System.out.println("Table order is filled.");

        Selector selector = new Selector();
        System.out.println("Select list of customers and their total amount of spent money (grouped by customer):");
        selector.select("SELECT c.id, c.first_name, c.last_name, SUM(o.price) AS spent_money " +
                "FROM customer AS c " +
                "LEFT JOIN public.order AS o " +
                "ON c.id=o.customer_id " +
                "GROUP BY c.id");

        System.out.println("Select most popular product:");
        selector.select("SELECT p.id, p.name, SUM(o.quantity) AS product_quantity " +
                "FROM product AS p " +
                "LEFT JOIN public.order AS o ON p.id=o.product_id " +
                "GROUP BY p.id " +
                "ORDER BY p.id DESC " +
                "LIMIT 1");

        System.out.println("Delete data from tables.");
        tableInitializer.deleteData("DELETE FROM public.order");
        tableInitializer.deleteData("DELETE FROM customer");
        tableInitializer.deleteData("DELETE FROM product");
    }
}
package org.geekhub.task1;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class Selector {

    public void select(String sqlRequest) throws SQLException {
        ConnectionProvider connectionProvider = new ConnectionProvider();

        try (Statement statement = connectionProvider.connection().createStatement();
             ResultSet resultSet = statement.executeQuery(sqlRequest)) {

            ResultSetMetaData rsmd = resultSet.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (resultSet.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = resultSet.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                }
                System.out.println("");
            }
        }
    }
}

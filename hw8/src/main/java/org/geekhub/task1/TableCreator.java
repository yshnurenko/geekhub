package org.geekhub.task1;

import java.sql.*;

public class TableCreator {

    public void createTable(String sqlRequest) throws SQLException {
        ConnectionProvider connectionProvider = new ConnectionProvider();
        try (Statement statement = connectionProvider.connection().createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }
}
package org.geekhub.task1;

import java.sql.SQLException;
import java.sql.Statement;

public class TableManager {

    private ConnectionProvider connectionProvider;

    public TableManager(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public void fillingTable(String sqlRequest) throws SQLException {
        try (Statement statement = connectionProvider.connection().createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }

    public void deleteData(String sqlRequest) throws SQLException {
        try (Statement statement = connectionProvider.connection().createStatement()) {
            statement.executeUpdate(sqlRequest);
        }
    }
}
package org.geekhub.task2.storage;

import org.geekhub.task2.objects.Entity;
import org.geekhub.task2.objects.Ignore;
import org.geekhub.task2.objects.Name;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        String sql = "SELECT * FROM " + "public." + clazz.getSimpleName().toLowerCase() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        try (Statement statement = connection.createStatement()) {
            String sql = "SELECT * FROM " + "public." + clazz.getSimpleName().toLowerCase();
            return extractResult(clazz, statement.executeQuery(sql));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        try (Statement statement = connection.createStatement()) {
            int i = statement.executeUpdate("DELETE FROM " + "public." + entity.getClass().getSimpleName().toLowerCase() + " WHERE id = " + entity.getId());
            if (i == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        if (entity.getId() == null) {
            int id = doSave(entity);
            entity.setId(id);
            return;
        }
        Entity e = get(entity.getClass(), entity.getId());
        if (e == null) {
            int id = doSave(entity);
            entity.setId(id);
        } else {
            int id = doUpdate(entity);
            entity.setId(id);
        }
    }

    private <T extends Entity> int doUpdate(T entity) throws Exception {
        Map<String, Object> map = prepareEntity(entity);
        String sql = updateEntitySql(map, entity, entity.getId());

        return executeSql(sql, map);
    }

    private <T extends Entity> int doSave(T entity) throws Exception {
        Map<String, Object> map = prepareEntity(entity);
        String sql = createEntitySql(map, entity);

        return executeSql(sql, map);
    }

    private int executeSql(String sql, Map<String, Object> map) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            int i = 1;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                statement.setObject(i++, entry.getValue());
            }
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            return generatedKeys.getInt(1);
        }
    }

    private <T extends Entity> String getTableName(T entity) {
        Name nameAnnotation = entity.getClass().getAnnotation(Name.class);
        String tableName;

        if (nameAnnotation != null) {
            tableName = nameAnnotation.value();
        } else tableName = entity.getClass().getSimpleName();

        return tableName;
    }

    private <T extends Entity> String createEntitySql(Map<String, Object> map, T entity) {
        String columnNamesCommaSeparated = String.join(",", new ArrayList<>(map.keySet()));
        String values = Stream.of(columnNamesCommaSeparated.split(","))
                .map(s -> "?")
                .collect(Collectors.joining(","));
        return format("INSERT INTO public.%s(%s) VALUES (%s)",
                getTableName(entity),
                columnNamesCommaSeparated,
                values);
    }

    private <T extends Entity> String updateEntitySql(Map<String, Object> map, T entity, Integer id) {
        StringBuilder setPart = new StringBuilder();
        for (String columnName : new ArrayList<>(map.keySet())) {
            setPart.append(columnName).append("=?").append(",");
        }
        return format("UPDATE public.%s SET %s WHERE id=%s",
                getTableName(entity),
                setPart.substring(0, setPart.lastIndexOf(",")),
                id);
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> data = new HashMap<>();

        for (Field field : entity.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Ignore.class)) continue;
            field.setAccessible(true);
            data.put(field.getName(), field.get(entity));
        }
        return data;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> result = new ArrayList<>();
        while (resultSet.next()) {
            T t = clazz.getDeclaredConstructor().newInstance();

            List<Field> fields = getFields(clazz);
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName();
                Ignore ignoreAnnotation = field.getAnnotation(Ignore.class);
                if (ignoreAnnotation != null) {
                    continue;
                }
                Object fieldValueRaw = resultSet.getObject(fieldName);

                field.set(t, fieldValueRaw);
            }
            result.add(t);
        }
        return result;
    }

    private <T extends Entity> List<Field> getFields(Class<T> clazz) {
        List<Field> result = new ArrayList<>();

        Class<?> i = clazz;
        while (i != null && i != Object.class) {
            Collections.addAll(result, i.getDeclaredFields());
            i = i.getSuperclass();
        }
        return result;
    }
}

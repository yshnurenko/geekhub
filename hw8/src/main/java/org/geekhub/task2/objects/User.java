package org.geekhub.task2.objects;

import java.math.BigDecimal;
import java.time.LocalDate;

@Name("user")
public class User extends Entity {
    private String name;
    private Integer age;
    private Boolean admin;
    private BigDecimal balance;

    @Ignore
    private LocalDate creationDate;

    public User() {
        this.creationDate = LocalDate.now();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = new BigDecimal(balance);
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }
}

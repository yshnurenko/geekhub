/*Search for tickets for a specific passenger*/
SELECT first_name, last_name, train_id, carriage, place, price
FROM tickets t
         INNER JOIN passengers p on t.passenger_id = p.id
WHERE first_name = 'Thomas'
  AND last_name = 'Little'
GROUP BY first_name, last_name, train_id, carriage, place, price

/*Determining the number of tickets for a specific trip*/
SELECT *,
       (SELECT COUNT(ticket_id)) as tickets_count
FROM trips tr
WHERE whence = 'Kansas'
  AND "where" = 'Mexico'
GROUP BY tr.id

/*Search for the number of passengers in a specific train*/
SELECT tr.name, COUNT(p.id) AS passengers_count
FROM tickets t
         INNER JOIN passengers p
                    ON t.passenger_id = p.id
         INNER JOIN trains tr
                    ON t.train_id = tr.id
WHERE tr.name = '642 IC+'
GROUP BY tr.name*

/*Ticket price purchased by a specific passenger*/
SELECT first_name, last_name, price
FROM tickets t
         INNER JOIN passengers p ON t.passenger_id = p.id
WHERE first_name = 'Victor'
  AND last_name = 'Bishop'
GROUP BY first_name, last_name, price

/*The number of tickets purchased in a particular train*/
SELECT tr.name, COUNT(train_id) AS tickets_count
FROM tickets t
         INNER JOIN trains tr ON t.train_id = tr.id
WHERE tr.name = '734 IC+'
GROUP BY tr.name

/*Update ticket price*/
UPDATE tickets SET price = 700 WHERE price = 650;

/*Refresh passenger place*/
UPDATE tickets SET place = 17 WHERE place = 16;

/*Update trip date*/
UPDATE trips SET departure_date = '2019-12-27' WHERE departure_date = '2019-12-26';

/*Remove passenger*/
DELETE FROM passengers WHERE first_name = 'John';

/*Delete ticket by passenger name*/
DELETE FROM tickets USING passengers
WHERE tickets.passenger_id= passengers.id AND passengers.first_name = 'John';
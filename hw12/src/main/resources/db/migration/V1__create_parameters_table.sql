CREATE TABLE IF NOT EXISTS "parameters" (
  "id"             SERIAL       NOT NULL,
  "name"           VARCHAR(50)  NOT NULL,
  "massage"        VARCHAR(100) NOT NULL,
  "rating"         INT          NOT NULL,
  PRIMARY KEY ("id")
);
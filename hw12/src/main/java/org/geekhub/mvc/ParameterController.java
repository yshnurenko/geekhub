package org.geekhub.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;

@Controller
public class ParameterController {

    private final FeedBackRepository feedBackRepository;

    @Autowired
    public ParameterController(FeedBackRepository feedBackRepository) {
        this.feedBackRepository = feedBackRepository;
    }

    @RequestMapping(value = "/guestBook", method = RequestMethod.GET)
    public String parametersGetPage(HttpServletRequest req, Model model) throws SQLException {
        return pagination(req, model);
    }

    @RequestMapping(value = "/guestBook", method = RequestMethod.POST)
    public String parametersPostPage(HttpServletRequest req, Model model) throws SQLException {
        feedBackRepository.addParameter(req.getParameter("name"), req.getParameter("massage"), req.getParameter("rating"));
        return pagination(req, model);
    }

    private String pagination(HttpServletRequest req, Model model) throws SQLException {
        int currentPage = 1;
        int recordsPerPage = 10;

        if (req.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(req.getParameter("currentPage"));
        }

        if (req.getParameter("recordsPerPage") != null) {
            recordsPerPage = Integer.parseInt(req.getParameter("recordsPerPage"));
        }

        List<ParameterModel> result = feedBackRepository.findParameters(recordsPerPage * currentPage, (currentPage - 1) * recordsPerPage);
        model.addAttribute("result", result);

        int nOfRecords = feedBackRepository.getNumberOfRows();
        int nOfPages = (int) Math.ceil(nOfRecords * 1.0 / recordsPerPage);

        model.addAttribute("nOfPages", nOfPages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("recordsPerPage", recordsPerPage);

        return "guestBook";
    }
}
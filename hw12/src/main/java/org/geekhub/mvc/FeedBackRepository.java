package org.geekhub.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class FeedBackRepository {

    private final DataSource dataSource;

    @Autowired
    public FeedBackRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<ParameterModel> findParameters(int limit, int offset) throws SQLException {

        ArrayList<ParameterModel> parameters = new ArrayList<>();

        String sql = "SELECT * FROM parameters ORDER BY date DESC LIMIT " + limit + " OFFSET " + offset;

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                ParameterModel parameterModel = new ParameterModel(
                        rs.getString("name"),
                        rs.getString("massage"),
                        rs.getString("rating")
                );
                parameters.add(parameterModel);
            }
        }
        return parameters;
    }

    public void addParameter(String name, String massage, String rating) throws SQLException {

        String sql = "INSERT INTO parameters (name, massage, rating, date) VALUES (?,?,?,?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, name);
            preparedStatement.setString(2, massage);
            preparedStatement.setInt(3, Integer.parseInt(rating));
            preparedStatement.setDate(4, Date.valueOf(LocalDate.now()));
            preparedStatement.executeUpdate();
        }
    }

    public int getNumberOfRows() throws SQLException {

        int numOfRows = 0;

        String sql = "SELECT COUNT(id) FROM parameters";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                numOfRows = rs.getInt(1);
            }
            return numOfRows;
        }
    }
}
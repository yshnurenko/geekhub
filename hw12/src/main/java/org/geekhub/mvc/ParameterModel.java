package org.geekhub.mvc;

public class ParameterModel {

    private String name;
    private String massage;
    private String rating;

    public ParameterModel(String name, String massage, String rating) {
        this.name = name;
        this.massage = massage;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public String getMassage() {
        return massage;
    }

    public String getRating() {
        return rating;
    }
}
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Login</title>
</head>
<body>
<div class="form">
    <h1>Sign in to GuestBook</h1><br>
    <form action="" method="POST" >
        <label>
            <input type="text" required placeholder="login" name="login">
        </label><br>
        <label>
            <input type="password" required placeholder="password" name="password">
        </label><br><br>
        <input class="button" type="submit" value="Sign in">
    </form>
</div>
</body>
</html>
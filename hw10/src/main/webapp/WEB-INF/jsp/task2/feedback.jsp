<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Guest Book</title>
</head>
<body>
<h1>Guest Book</h1>
<form action="feedback" method="POST">
    <br>Name:<br/>
    <label>
        <input type="text" value="<%= session.getAttribute("user").toString() %>" name=name>
    </label>
    <br>Massage:<br/>
    <label>
        <textarea rows=5 cols=30 name=massage></textarea>
    </label>
    <br>Rating:<br/>
    <label>
        <select name=rating>
            <option value=1>1</option>
            <option value=2>2</option>
            <option value=3>3</option>
            <option value=4>4</option>
            <option value=5>5</option>
        </select>
    </label>
    <br><input type=submit value=Submit><br/>
</form>

<br>
<table border="1" cellpadding="5" cellspacing="5">
    <tr>
        <th>Name</th>
        <th>Massage</th>
        <th>Rating</th>
    </tr>

    <c:forEach items="${requestScope.result}" var="parameterService">
        <tr>
            <td>${parameterService.name}</td>
            <td>${parameterService.massage}</td>
            <td>${parameterService.rating}</td>
        </tr>
    </c:forEach>
</table>
<br/>

<c:if test="${requestScope.currentPage!=1}">
    <td>
        <a href="feedback?recordsPerPage=${requestScope.recordsPerPage}&currentPage=${requestScope.currentPage - 1}">Previous</a>
    </td>
</c:if>

<table border="1" cellpadding="5" cellspacing="5">
    <tr>
        <c:forEach begin="1" end="${requestScope.nOfPages}" var="i">
            <c:choose>
                <c:when test="${requestScope.currentPage eq i}">
                    <td>${i}</td>
                </c:when>
                <c:otherwise>
                    <td>
                        <a href="feedback?recordsPerPage=${requestScope.recordsPerPage}&currentPage=${i}">${i}</a>
                    </td>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </tr>
</table>

<c:if test="${requestScope.currentPage lt requestScope.nOfPages}">
    <td>
        <a href="feedback?recordsPerPage=${requestScope.recordsPerPage}&currentPage=${requestScope.currentPage + 1}">Next</a>
    </td>
</c:if>

<br>
<form action="logout" method="POST">
    <input type="submit" value="Logout"/>
</form>
<br/>
</body>
</html>
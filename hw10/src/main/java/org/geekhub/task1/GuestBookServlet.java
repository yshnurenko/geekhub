package org.geekhub.task1;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class GuestBookServlet extends HttpServlet {

    List<ParameterCreator> parameters = new ArrayList<>();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        try (PrintWriter writer = response.getWriter()) {
            writer.write("<h1>Guest Book: </h1>");
            writer.write(getForm());
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        try (PrintWriter writer = response.getWriter()) {
            writer.write("<h1>Guest Book: </h1>");
            writer.write(getForm());

            parameters.add(new ParameterCreator(request.getParameter("name"), request.getParameter("massage"), request.getParameter("rating")));
            parameters.sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
            for (ParameterCreator parameterCreator : parameters) {
                writer.write("<br>Name:" + parameterCreator.getName() + "; Massage:" + parameterCreator.getMassage() + "; Rating:" + parameterCreator.getRating() + ".<br/>");
            }
        }
    }

    private String getForm() {
        String name = "<br>Name:<br/>" +
                "<input type=text name=name />";
        String massage = "<br>Massage:<br/>" +
                "<textarea rows=5 cols=30 name=massage></textarea>";
        String rating = "<br>Rating:<br/>" +
                "<select name=rating>" +
                "    <option value=1>1</option>" +
                "    <option value=2>2</option>" +
                "    <option value=3>3</option>" +
                "    <option value=4>4</option>" +
                "    <option value=5>5</option>" +
                "</select>";
        String submit = "<br><input type=submit value=Submit><br/>";

        return "<form action=/guestBook method='post'>" + name + massage + rating + submit + "</form>";
    }
}

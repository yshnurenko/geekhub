package org.geekhub.task2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@WebServlet("/feedback")
public class GuestBookServlet extends HttpServlet {

    private List<ParameterService> parameters = new CopyOnWriteArrayList<>();

    public GuestBookServlet() {
        for (int i = 0; i < 20; i++) {
            parameters.add(new ParameterService("name" + i, "comment" + i, ""));
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        pagination(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        resp.setContentType("text/html");
        addParameter(req.getParameter("name"), req.getParameter("massage"), req.getParameter("rating"));
        pagination(req, resp);
    }

    private void addParameter(String name, String massage, String rating) {
        parameters.add(new ParameterService(name, massage, rating));
        parameters.sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
    }

    private void pagination(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        int currentPage = 1;
        int recordsPerPage = 5;

        if (req.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(req.getParameter("currentPage"));
        }

        if (req.getParameter("recordsPerPage") != null) {
            recordsPerPage = Integer.parseInt(req.getParameter("recordsPerPage"));
        }

        List<ParameterService> result = parameters.subList((currentPage - 1) * recordsPerPage, recordsPerPage * currentPage);
        req.setAttribute("result", result);

        int nOfRecords = parameters.size();
        int nOfPages = (int) Math.ceil(nOfRecords * 1.0 / recordsPerPage);

        req.setAttribute("nOfPages", nOfPages);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("recordsPerPage", recordsPerPage);

        req.getRequestDispatcher("WEB-INF/jsp/task2/feedback.jsp").forward(req, resp);
    }
}
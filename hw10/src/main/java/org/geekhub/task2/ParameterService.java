package org.geekhub.task2;

import java.time.LocalDateTime;

public class ParameterService {

    private String name;
    private String massage;
    private String rating;
    private LocalDateTime date = LocalDateTime.now();

    public ParameterService(String name, String massage, String rating) {
        this.name = name;
        this.massage = massage;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public String getMassage() {
        return massage;
    }

    public String getRating() {
        return rating;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
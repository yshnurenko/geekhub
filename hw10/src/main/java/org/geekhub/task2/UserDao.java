package org.geekhub.task2;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class UserDao {

    private List<User> store = new CopyOnWriteArrayList<>();

    UserDao() {
        store.add(new User("John", "123456"));
        store.add(new User("Mario", "654321"));
    }

    public User getUserByLoginPassword(String login, String password) {
        User result = null;
        for (User user : store) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                result = user;
            }
        }
        return result;
    }

    public boolean userIsExist(String login, String password) {
        boolean result = false;
        for (User user : store) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
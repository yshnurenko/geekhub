package org.geekhub.hw1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        do {
            System.out.println("Please enter the phone number: ");
            input = scanner.nextLine();
            boolean valid = validate(input);
            if (valid) {
                String output = calculateSum(input);
                printResult(output);
                return;
            }
        } while (!input.equals("exit"));
    }

    private static boolean validate(String input) {
        if (!input.matches("^[0-9]*$")) {
            System.out.println("Phone number is incorrect.");
            return false;
        } else if (input.startsWith("380") && input.length() == 12) {
            System.out.println("Phone number is correct.");
            return true;
        } else if (input.startsWith("00380") && input.length() == 14) {
            System.out.println("Phone number is correct.");
            return true;
        } else if (input.startsWith("050") || input.startsWith("099") ||
                input.startsWith("097") || input.startsWith("067") ||
                input.startsWith("096") || input.startsWith("066") ||
                input.startsWith("095") && input.length() == 10) {
            System.out.println("Phone number is correct.");
            return true;
        } else System.out.println("Phone number is incorrect.");
        return false;
    }

    private static String calculate(String input) {
        int sum = 0;
        final char[] chars = input.toCharArray();
        int[] ints = new int[chars.length];
        for (int i = 0; i < chars.length; i++) {
            ints[i] = Character.getNumericValue(chars[i]);
        }
        for (int anInt : ints) {
            sum = sum + anInt;
        }
        return String.valueOf(sum);
    }

    private static void printResult(String sum) {
        if (sum.length() > 1) {
            return;
        }
        switch (sum) {
            case "1":
                System.out.println("Final result is: One");
                break;
            case "2":
                System.out.println("Final result is: Two");
                break;
            case "3":
                System.out.println("Final result is: Three");
                break;
            case "4":
                System.out.println("Final result is: Four");
                break;
            default:
                System.out.println("Final result is: " + sum);
                break;
        }
    }

    private static String calculateSum(String input) {
        int step = 1;
        do {
            input = calculate(input);
            System.out.println(step + " round of calculation, sum is: " + input);
        } while (input.length() > 1);
        return input;
    }
}

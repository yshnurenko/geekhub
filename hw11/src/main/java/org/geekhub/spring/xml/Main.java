package org.geekhub.spring.xml;

import org.geekhub.spring.xml.beans.Car;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("/beans.xml");

        Car car = ctx.getBean("car", Car.class);
        System.out.println("Car:" + car);
    }
}
package org.geekhub.spring.javabased.configuration;

import org.geekhub.spring.javabased.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class AppConfig {

    @Bean
    public Car carBean() {
        return new Car(engineBean(), wheelListBean());
    }

    @Bean
    public Engine engineBean() {
        return new Engine();
    }

    @Bean
    public List<Wheel> wheelListBean() {
        List<Wheel> wheels = new ArrayList<>();
        wheels.add(wheelBean());
        wheels.add(wheelBean());
        wheels.add(wheelBean());
        wheels.add(wheelBean());
        return wheels;
    }

    @Bean
    public Wheel wheelBean() {
        return new Wheel(summerTyreBean());
    }

    @Bean
    public SummerTyre summerTyreBean() {
        return new SummerTyre();
    }

    @Bean
    public WinterTyre winterTyreBean() {
        return new WinterTyre();
    }
}
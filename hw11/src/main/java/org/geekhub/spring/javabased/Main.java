package org.geekhub.spring.javabased;

import org.geekhub.spring.javabased.configuration.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        Car car = ctx.getBean("carBean", Car.class);
        System.out.println("Car:" + car);
    }
}
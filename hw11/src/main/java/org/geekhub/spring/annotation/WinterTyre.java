package org.geekhub.spring.annotation;

import org.springframework.stereotype.Component;

@Component
public class WinterTyre extends Tyre {

    @Override
    public String toString() {
        return super.toString() + "WinterTyre";
    }
}
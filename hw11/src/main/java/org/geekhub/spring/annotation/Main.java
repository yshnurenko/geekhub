package org.geekhub.spring.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext("org.geekhub.spring.annotation");

        Car car = context.getBean("car", Car.class);
        System.out.println("Car:" + car);
    }
}
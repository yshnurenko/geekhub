package org.geekhub.spring.annotation;

import org.springframework.stereotype.Component;

@Component
public class Engine {

    private int capacity;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "{" +
                "capacity=" + capacity +
                '}';
    }
}
package org.geekhub.spring.annotation;

import org.springframework.stereotype.Component;

@Component
public class SummerTyre extends Tyre {

    @Override
    public String toString() {
        return super.toString() + "SummerTyre";
    }
}
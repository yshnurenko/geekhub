package org.geekhub.spring.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Car {

    private Engine engine;
    private List<Wheel> wheels;
    ApplicationContext context;

    @Autowired
    public Car(Engine engine, List<Wheel> wheels, ApplicationContext context) {
        this.engine = engine;
        this.wheels = wheels;
        this.context = context;
        for (int i = 1; i < 4; i++) {
            wheels.add(createWheelInstance());
        }
    }

    private Wheel createWheelInstance() {
        return context.getBean("wheel", Wheel.class);
    }

    @Override
    public String toString() {
        return "{" +
                "engine=" + engine +
                ", wheels=" + wheels +
                '}';
    }
}
package org.geekhub.list.linked;

import org.geekhub.list.List;

import java.util.Iterator;
import java.util.Objects;

public class LinkedList<E> implements List<E> {

    private Node<E> head;
    private Node<E> tail;

    private int size = 0;

    @Override
    public boolean add(E element) {
        Node<E> node = new Node<>(element, null);
        if (tail == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
        size++;

        return true;
    }

    @Override
    public boolean add(int index, E element) {
        Node<E> node = new Node<>(element, null);
        Node<E> current = head;

        if (index == 0) {
            node.setNext(head);
            head = node;
        } else {
            for (int i = 1; i < index; i++) {
                current = current.getNext();
            }
            node.setNext(current.getNext());
            current.setNext(node);
        }
        size++;

        return true;
    }

    @Override
    public boolean addAll(List<E> elements) {
        for (E element : elements) {
            add(element);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, List<E> elements) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        int numNew = elements.size();
        if (numNew == 0)
            return false;

        Node<E> current = getNode(index);
        Node<E> prevNode = getNode(index - 1);

        for (int i = elements.size() - 1; i >= 0; i--) {
            E e = elements.get(i);
            if (current == null) {
                Node<E> newNode = new Node<>(e, head.next);

                prevNode.setNext(newNode);

                head = prevNode;
            } else {
                current = new Node<>(e, current);

                if (prevNode == null) {
                    head = current;
                } else {
                    head.next = current;
                }
            }
            size++;
        }
        return true;
    }

    @Override
    public boolean clear() {
        for (Node<E> x = head; x != null; ) {
            Node<E> next = x.next;
            x.element = null;
            x.next = null;
            x = next;
        }
        tail = head = null;
        size = 0;

        return true;
    }

    @Override
    public E remove(int index) {
        Node<E> current = head;
        int jump;
        if (index > size || index < 1) {
            return null;
        } else {
            jump = 0;
            while (jump < index - 1) {
                current = current.next;
                jump++;
            }
            current.next = current.next.next;
            tail = current;
            size--;
        }
        return null;
    }

    @Override
    public E remove(E element) {
        Node<E> previous = null;
        Node<E> current = head;

        while (current != null) {
            if (current.element.equals(element)) {
                if (previous != null) {
                    previous.next = current.next;

                    if (current.next == null) {
                        head = previous;
                    }
                } else {
                    tail = tail.next;

                    if (tail == null) {
                        head = null;
                    }
                }
                size--;
            }
            previous = current;
            current = current.next;
        }
        return null;
    }

    @Override
    public E get(int index) {
        return Objects.requireNonNull(getNode(index)).element;
    }

    private Node<E> getNode(int index) {
        if (index > size || index < 0)
            return null;

        Node<E> currentNode = head;
        for (int i = 1; i <= index; i++) {
            currentNode = currentNode.getNext();
        }
        return currentNode;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public int indexOf(E element) {
        int index = 0;
        if (element == null) {
            for (Node<E> x = head; x != null; x = x.next) {
                if (x.element == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = head; x != null; x = x.next) {
                if (element.equals(x.element))
                    return index;
                index++;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<>(head);
    }
}
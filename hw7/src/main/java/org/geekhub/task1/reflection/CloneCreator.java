package org.geekhub.task1.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CloneCreator {

    public static <T> void clone(T object) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class clazz = object.getClass();
        T newEntity = (T) object.getClass().getDeclaredConstructor().newInstance();

        while (clazz != null) {
            cloneFields(object, newEntity, clazz);
            clazz = clazz.getSuperclass();
        }
    }

    private static <T> void cloneFields(T object, T newObject, Class<?> clazz) throws IllegalAccessException {
        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        for (Field field : fields) {
            field.setAccessible(true);
            field.set(newObject, field.get(object));
        }
        System.out.println(newObject.toString());
    }
}

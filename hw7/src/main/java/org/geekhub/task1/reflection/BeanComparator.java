package org.geekhub.task1.reflection;

import java.lang.reflect.Field;
import java.util.Arrays;

public class BeanComparator {

    public static <T, V> void compare(T firstObject, V secondObject) throws IllegalAccessException {
        Class firstClass = firstObject.getClass();
        Class secondClass = secondObject.getClass();

        Field[] firstFields = firstClass.getDeclaredFields();
        Field[] secondFields = secondClass.getDeclaredFields();

        Object[][] data = new Object[firstFields.length][4];

        for (int i = 0; i < firstFields.length; i++) {
            firstFields[i].setAccessible(true);
            secondFields[i].setAccessible(true);
            data[i][0] = firstFields[i].getName();
            data[i][1] = firstFields[i].get(firstObject);

            for (Field secondField : secondFields) {
                if (firstFields[i].getName().equals(secondField.getName())) {
                    data[i][2] = secondField.get(secondObject);
                    data[i][3] = (firstFields[i].get(firstObject).equals(secondField.get(secondObject))) ? "+" : "-";
                    break;
                } else {
                    data[i][2] = "-";
                    data[i][3] = "-";
                }
            }
        }
        System.out.println(Arrays.deepToString(data));
    }
}

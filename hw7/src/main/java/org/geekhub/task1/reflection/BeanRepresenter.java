package org.geekhub.task1.reflection;

import java.lang.reflect.Field;
import java.util.Arrays;

public class BeanRepresenter {

    private BeanRepresenter() {
    }

    public static <T> void represent(T object) throws IllegalAccessException {
        Class reflection = object.getClass();
        Field[] fields = reflection.getDeclaredFields();

        Object[][] data = new Object[fields.length][2];

        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            data[i][0] = fields[i].getName();
            data[i][1] = fields[i].get(object).toString();
        }
        System.out.println(Arrays.deepToString(data));
    }
}

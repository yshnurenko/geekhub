package org.geekhub.task1.testclass;

public class Human {
    private double height;
    private String gender;
    private int age;
    private double weight;

    public Human() {
    }

    public Human(double height, String gender, int age, double weight) {
        this.height = height;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Human{" +
                "height=" + height +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }
}

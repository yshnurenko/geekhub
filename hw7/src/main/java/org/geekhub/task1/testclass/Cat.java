package org.geekhub.task1.testclass;

public class Cat {
    private String color;
    private int age;
    private int legCount;
    private int fullLength;

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }
}

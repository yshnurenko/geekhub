package org.geekhub.task1.testclass;

public class Car {
    private String color;
    private int maxSpeed;
    private String type;
    private String model;
    private int maxCount;

    public Car(String color, int maxSpeed, String type, String model, int maxCount) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
        this.maxCount = maxCount;
    }
}

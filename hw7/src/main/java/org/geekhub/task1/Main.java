package org.geekhub.task1;

import org.geekhub.task1.reflection.BeanRepresenter;
import org.geekhub.task1.reflection.BeanComparator;
import org.geekhub.task1.testclass.Car;
import org.geekhub.task1.testclass.Cat;
import org.geekhub.task1.testclass.Human;
import org.geekhub.task1.reflection.CloneCreator;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Cat testCat = new Cat("black", 3, 4, 35);
        Car testCar = new Car("black", 190, "sedan", "rx-7", 120);
        Human testHuman = new Human(180, "male", 22, 75);

        BeanRepresenter.represent(testCat);

        CloneCreator.clone(testHuman);

        Car testCar2 = new Car("black", 200, "coupe", "rx-8", 120);
        BeanComparator.compare(testCar2, testCar);
        BeanComparator.compare(testCat, testCar);
    }
}

package org.geekhub.task2.test;

import org.geekhub.task2.json.adapter.ColorAdapter;
import org.geekhub.task2.json.adapter.UseDataAdapter;

import java.awt.*;

public class Paw {

    private Integer length;

    @UseDataAdapter(ColorAdapter.class)
    private Color color;

    Paw(Integer length, Color color) {
        this.length = length;
        this.color = color;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
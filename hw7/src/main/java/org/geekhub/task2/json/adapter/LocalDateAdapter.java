package org.geekhub.task2.json.adapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Converts object of type java.time.LocalDate to String by using ISO 8601 format
 */
public class LocalDateAdapter implements JsonDataAdapter<LocalDate> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    public Object toJson(LocalDate date) {
        return date.format(formatter);
    }
}

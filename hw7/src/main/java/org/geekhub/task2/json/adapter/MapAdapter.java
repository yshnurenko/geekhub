package org.geekhub.task2.json.adapter;

import org.geekhub.task2.json.JsonSerializer;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Set;


/**
 * Converts all objects that extends java.util.Map to JSONObject.
 */
public class MapAdapter implements JsonDataAdapter<Map> {

    @Override
    public Object toJson(Map map) throws JSONException {
        JSONObject json = new JSONObject();
        for (Map.Entry entry : (Set<Map.Entry>) map.entrySet()) {
            Object key = entry.getKey();
            Object value = entry.getValue();

            json.put((String) key, JsonSerializer.serialize(value));
        }
        return json;
    }
}

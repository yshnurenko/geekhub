package org.geekhub.task1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {
        fileFilling();
        encryption();
    }

    private static void fileFilling() {
        File file = new File("hw6/src/main/resources/task1", "test.txt");
        try (FileWriter writer = new FileWriter(file, false)) {

            String text = "Direction, test, book, homework, description, refrigerator, vegetables";
            writer.write(text);

        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    private static void encryption() throws IOException {
        String content = new String(Files.readAllBytes(Paths.get("hw6/src/main/resources/task1", "test.txt")), StandardCharsets.UTF_8);
        String[] splitContent = content.split("[.,\\s-]");

        StringBuilder result = new StringBuilder();
        for (String s : splitContent) {
            String convert = stringConvert(s);
            if (convert.isEmpty()) continue;
            result.append(convert).append(" ");
        }
        File file = new File("hw6/src/main/resources/task1", "result.txt");
        FileWriter writer = new FileWriter(file, false);
        writer.write(result.toString());
        writer.close();
    }

    private static String stringConvert(String content) {
        if (content.length() >= 10) {
            char firstLetter = content.charAt(0);
            char lastLetter = content.charAt(content.length() - 1);
            int count = content.length() - 2;

            return "" + firstLetter + count + lastLetter;
        } else {
            return "";
        }
    }
}

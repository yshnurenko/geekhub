package org.geekhub.task2;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

public class FileWorker {

    void zippedAudio(String sourceFile) {
        try (FileOutputStream fos = new FileOutputStream("audios.zip");
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            List<String> filteredAudios = FileFilter.filterFile(sourceFile);
            List<String> resultAudios = filteredAudios.stream()
                    .filter(f -> f.endsWith(".mp3") || f.endsWith(".wav") || f.endsWith(".wma"))
                    .collect(Collectors.toList());

            resultAudios.forEach(System.out::println);

            for (String resultAudio : resultAudios) {
                ZipMaker.writeToZipFile(resultAudio, zos);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void zippedVideo(String sourceFile) {
        try (FileOutputStream fos = new FileOutputStream("videos.zip");
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            List<String> filteredVideos = FileFilter.filterFile(sourceFile);
            List<String> resultVideos = filteredVideos.stream()
                    .filter(f -> f.endsWith(".avi") || f.endsWith(".mp4") || f.endsWith(".flv"))
                    .collect(Collectors.toList());

            resultVideos.forEach(System.out::println);

            for (String resultVideo : resultVideos) {
                ZipMaker.writeToZipFile(resultVideo, zos);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void zippedImages(String sourceFile) {
        try (FileOutputStream fos = new FileOutputStream("images.zip");
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            List<String> filteredImages = FileFilter.filterFile(sourceFile);
            List<String> resultImages = filteredImages.stream()
                    .filter(f -> f.endsWith(".jpg") || f.endsWith(".jpeg") || f.endsWith(".gif") || f.endsWith(".png"))
                    .collect(Collectors.toList());

            resultImages.forEach(System.out::println);

            for (String resultImage : resultImages) {
                ZipMaker.writeToZipFile(resultImage, zos);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

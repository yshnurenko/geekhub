package org.geekhub.task2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter directory path: ");
        String sourceFile = scanner.nextLine();

        FileWorker fileWorker = new FileWorker();
        fileWorker.zippedAudio(sourceFile);
        fileWorker.zippedVideo(sourceFile);
        fileWorker.zippedImages(sourceFile);
    }
}

package org.geekhub.hw3;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();
        LegalEntity legalEntity = new LegalEntity(1, "PRCompany", false, false);
        Individual individual = new Individual(2, "John");
        Individual individual1 = new Individual(3, "Nick");
        Individual individual2 = new Individual(4, "Nickolas");
        LegalEntity legalEntity1 = new LegalEntity(5, "Nike", true, true);
        LegalEntity legalEntity2 = new LegalEntity(6, "GeekHub", true, true);
        bank.addClient(legalEntity);
        bank.addClient(individual);
        bank.addClient(individual1);
        bank.addClient(individual2);
        bank.addClient(legalEntity1);
        bank.addClient(legalEntity2);
        bank.viewInformation();
        bank.addCustomerAsset(legalEntity, new PreciousMetal("Gold", 2, 100));
        bank.addCustomerAsset(individual, new Deposit(Currency.DOLLAR, LocalDate.now(), 1000, 3, 100, 1));
        bank.addCustomerAsset(individual1, new DollarBill(10000));
        bank.addCustomerAsset(individual2, new DollarBill(3220000));
        bank.addCustomerAsset(legalEntity1, new Deposit(Currency.UAH, LocalDate.now(), 60000, 2, 300, 2));
        bank.addCustomerAsset(legalEntity2, new Deposit(Currency.EUR, LocalDate.now(), 100000, 3, 200, 1));
        System.out.println("All bank assets: " + bank.amountFunds());
    }
}

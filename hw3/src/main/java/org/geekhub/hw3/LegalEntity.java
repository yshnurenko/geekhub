package org.geekhub.hw3;

public class LegalEntity extends Individual {
    private int id;
    private String name;
    private boolean stateRegistration;
    private boolean constituentDocuments;

    LegalEntity(int id, String name, boolean stateRegistration, boolean constituentDocuments) {
        super(id, name);
        this.id = id;
        this.name = name;
        this.stateRegistration = stateRegistration;
        this.constituentDocuments = constituentDocuments;
    }

    @Override
    public String toString() {
        return "{" +
                "id = " + id +
                ", name = " + name +
                ", stateRegistration = " + stateRegistration +
                ", constituentDocuments = " + constituentDocuments +
                '}';
    }
}

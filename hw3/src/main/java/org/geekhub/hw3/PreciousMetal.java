package org.geekhub.hw3;

public class PreciousMetal extends BankAsset {
    private String metalType;
    private double mass;
    private double priceGram;

    PreciousMetal(String metalType, double mass, double priceGram) {
        this.metalType = metalType;
        this.mass = mass;
        this.priceGram = priceGram;
    }

    @Override
    double getActives() {
        return mass * priceGram;
    }

    @Override
    public String toString() {
        return "{" +
                "metalType = " + metalType +
                ", mass = " + mass +
                ", priceGram = " + priceGram +
                '}';
    }
}

package org.geekhub.hw3;

import java.time.LocalDate;

public class Deposit extends BankAsset {
    private Currency currency;
    private LocalDate startDate;
    private double amountMoney;
    private double annualPercentage;
    private double startingPrice;
    private double amountYears;

    Deposit(Currency currency, LocalDate startDate, double amountMoney, double annualPercentage, double startingPrice, double amountYears) {
        this.currency = currency;
        this.startDate = startDate;
        this.amountMoney = amountMoney;
        this.annualPercentage = annualPercentage;
        this.startingPrice = startingPrice;
        this.amountYears = amountYears;
    }

    @Override
    double getActives() {
        return startingPrice + startingPrice * annualPercentage * amountYears;
    }

    @Override
    public String toString() {
        return "{" +
                "currency = " + currency +
                ", startDate = " + startDate +
                ", amountMoney = " + amountMoney +
                ", annualPercentage = " + annualPercentage +
                ", startingPrice = " + startingPrice +
                ", amountYears = " + amountYears +
                '}';
    }
}

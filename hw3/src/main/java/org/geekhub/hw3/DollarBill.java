package org.geekhub.hw3;

public class DollarBill extends BankAsset {
    private double amountMoney;

    DollarBill(double amountMoney) {
        this.amountMoney = amountMoney;
    }

    @Override
    double getActives() {
        return amountMoney;
    }

    @Override
    public String toString() {
        return "{" +
                "amountMoney = " + amountMoney +
                '}';
    }
}

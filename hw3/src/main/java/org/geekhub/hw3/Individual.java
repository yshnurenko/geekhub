package org.geekhub.hw3;

import java.util.LinkedList;

class Individual {
    private int id;
    private String name;

    private LinkedList<BankAsset> accounts = new LinkedList<>();

    Individual(int id, String name) {
        this.id = id;
        this.name = name;
    }

    void addBankAsset(BankAsset bankAsset) {
        accounts.add(bankAsset);
    }

    double returnAsset() {
        double result = 0;
        for (BankAsset account : accounts) {
            result += account.getActives();
        }
        return result;
    }

    int getId() {
        return id;
    }

    String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "{" +
                "id = " + id +
                ", name = " + name +
                '}';
    }
}

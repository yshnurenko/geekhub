package org.geekhub.hw3;

import java.util.LinkedList;

class Bank {

    private LinkedList<Individual> individuals = new LinkedList<>();

    void addClient(Individual individual) {
        individuals.add(individual);
    }

    void viewInformation() {
        System.out.println("Clients :");
        for (Individual client : individuals) {
            System.out.println(client);
        }
    }

    void addCustomerAsset(Individual individual, BankAsset bankAsset) {
        for (Individual client : individuals) {
            if (individual.getId() == client.getId() && individual.getName().equals(client.getName())) {
                individual.addBankAsset(bankAsset);
                System.out.println("Client: " + client + " Asset: " + bankAsset);
            }
        }
    }

    double amountFunds() {
        double result = 0;
        for (Individual individual : individuals) {
            result += individual.returnAsset();
        }
        return result;
    }
}



CREATE TABLE movie (
    id        SERIAL PRIMARY KEY,
    name      VARCHAR NOT NULL,
    year      INT NOT NULL,
    genre     VARCHAR(6) NOT NULL,
    language  VARCHAR(10) NOT NULL
);
CREATE TABLE comment (
    id         SERIAL PRIMARY KEY,
    name       VARCHAR NOT NULL,
    massage    VARCHAR NOT NULL,
    rating     INTEGER NOT NULL,
    movie_id   INT NOT NULL REFERENCES movie (id)
);
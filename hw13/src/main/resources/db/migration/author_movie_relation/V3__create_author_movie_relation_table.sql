CREATE TABLE author_movie_relation (
    author_id     INT NOT NULL REFERENCES author (id),
    movie_id      INT NOT NULL REFERENCES movie (id)
);

package com.geekhub.movieblog.download;

import com.geekhub.movieblog.author.Author;
import com.geekhub.movieblog.author.AuthorService;
import com.geekhub.movieblog.movie.Movie;
import com.geekhub.movieblog.movie.MovieService;
import com.geekhub.movieblog.movie.comment.CommentService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

@Component
public class PdfGenerator {

    private final MovieService movieService;
    private final AuthorService authorService;
    private final CommentService commentService;

    @Autowired
    public PdfGenerator(MovieService movieService, AuthorService authorService, CommentService commentService) {
        this.movieService = movieService;
        this.authorService = authorService;
        this.commentService = commentService;
    }

    public ByteArrayOutputStream generatePdfAuthors() throws DocumentException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Document document = new Document();

        PdfWriter writer = PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();

        PdfPTable table = new PdfPTable(4);
        table.setWidthPercentage(100);
        table.setSpacingBefore(10f);
        table.setSpacingAfter(10f);

        float[] columnWidths = {1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell id = new PdfPCell(new Paragraph("Id"));
        PdfPCell name = new PdfPCell(new Paragraph("Name"));
        PdfPCell gender = new PdfPCell(new Paragraph("Gender"));
        PdfPCell movieCount = new PdfPCell(new Paragraph("Movie count"));

        table.addCell(id);
        table.addCell(name);
        table.addCell(gender);
        table.addCell(movieCount);

        List<Author> authors = authorService.getAllAuthors();
        Map<Integer, Integer> movieCounts = authorService.getMovieCountByAuthorId();

        for (Author author : authors) {
            PdfPCell idData = new PdfPCell(new Paragraph(author.getId().toString()));
            PdfPCell nameData = new PdfPCell(new Paragraph(author.getName()));
            PdfPCell genderData = new PdfPCell(new Paragraph(String.valueOf(author.getGender())));
            PdfPCell movieCountData = new PdfPCell(new Paragraph(movieCounts.get(author.getId()).toString()));

            table.addCell(idData);
            table.addCell(nameData);
            table.addCell(genderData);
            table.addCell(movieCountData);
        }

        document.add(table);

        document.close();
        writer.close();

        return byteArrayOutputStream;
    }

    public ByteArrayOutputStream generatePdfMovies() throws DocumentException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Document document = new Document();

        PdfWriter writer = PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();

        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(100);
        table.setSpacingBefore(10f);
        table.setSpacingAfter(10f);

        float[] columnWidths = {1f, 1f, 1f, 1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);

        PdfPCell id = new PdfPCell(new Paragraph("Id"));
        PdfPCell name = new PdfPCell(new Paragraph("Name"));
        PdfPCell year = new PdfPCell(new Paragraph("Year"));
        PdfPCell genre = new PdfPCell(new Paragraph("Genre"));
        PdfPCell language = new PdfPCell(new Paragraph("Language"));
        PdfPCell commentCount = new PdfPCell(new Paragraph("Comment count"));
        PdfPCell averageRating = new PdfPCell(new Paragraph("Average rating"));

        table.addCell(id);
        table.addCell(name);
        table.addCell(year);
        table.addCell(genre);
        table.addCell(language);
        table.addCell(commentCount);
        table.addCell(averageRating);

        List<Movie> movies = movieService.getAllMovies();

        for (Movie movie : movies) {
            PdfPCell idData = new PdfPCell(new Paragraph(movie.getId().toString()));
            PdfPCell nameData = new PdfPCell(new Paragraph(movie.getName()));
            PdfPCell yearData = new PdfPCell(new Paragraph(String.valueOf(movie.getYear())));
            PdfPCell genreData = new PdfPCell(new Paragraph(String.valueOf(movie.getGenre())));
            PdfPCell languageData = new PdfPCell(new Paragraph(String.valueOf(movie.getLanguage())));
            PdfPCell commentCountData = new PdfPCell(new Paragraph(commentService.getCommentsCount(movie.getId()).toString()));
            PdfPCell averageRatingData = new PdfPCell(new Paragraph(commentService.getAverageRating(movie.getId()).toString()));

            table.addCell(idData);
            table.addCell(nameData);
            table.addCell(yearData);
            table.addCell(genreData);
            table.addCell(languageData);
            table.addCell(commentCountData);
            table.addCell(averageRatingData);
        }

        document.add(table);

        document.close();
        writer.close();

        return byteArrayOutputStream;
    }
}

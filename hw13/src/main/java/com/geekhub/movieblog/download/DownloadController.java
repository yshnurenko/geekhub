package com.geekhub.movieblog.download;

import com.itextpdf.text.DocumentException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Controller
@RequestMapping("/download")
public class DownloadController {

    private final ExcelGenerator excelGenerator;
    private final PdfGenerator pdfGenerator;
    private final WordGenerator wordGenerator;

    @Autowired
    public DownloadController(ExcelGenerator excelGenerator, PdfGenerator pdfGenerator, WordGenerator wordGenerator) {
        this.excelGenerator = excelGenerator;
        this.pdfGenerator = pdfGenerator;
        this.wordGenerator = wordGenerator;
    }

    @PostMapping("/author/excel")
    public void downloadAuthorTableToExcel(HttpServletResponse response) throws IOException {
        Workbook excelDocument = excelGenerator.generateExcelAuthors();
        response.setHeader("Content-Disposition", "attachment; filename=Authors.xls");
        OutputStream os = response.getOutputStream();
        excelDocument.write(os);
        os.flush();
        os.close();
    }

    @PostMapping("/movie/excel")
    public void downloadMovieTableToExcel(HttpServletResponse response) throws IOException {
        Workbook excelDocument = excelGenerator.generateExcelMovies();
        response.setHeader("Content-Disposition", "attachment; filename=Movies.xls");
        OutputStream os = response.getOutputStream();
        excelDocument.write(os);
        os.flush();
        os.close();
    }

    @PostMapping("/author/word")
    public void downloadAuthorTableToWord(HttpServletResponse response) throws IOException {
        XWPFDocument wordDocument = wordGenerator.generateWordAuthors();
        response.setHeader("Content-Disposition", "attachment; filename=Authors.doc");
        OutputStream os = response.getOutputStream();
        wordDocument.write(os);
        os.flush();
        os.close();
    }

    @PostMapping("/movie/word")
    public void downloadMovieTableToWord(HttpServletResponse response) throws IOException {
        XWPFDocument wordDocument = wordGenerator.generateWordMovies();
        response.setHeader("Content-Disposition", "attachment; filename=Movies.doc");
        OutputStream os = response.getOutputStream();
        wordDocument.write(os);
        os.flush();
        os.close();
    }

    @PostMapping("/author/pdf")
    public void downloadAuthorTableToPdf(HttpServletResponse response) throws IOException, DocumentException {
        ByteArrayOutputStream byteArrayOutputStream = pdfGenerator.generatePdfAuthors();
        response.setHeader("Content-Disposition", "attachment;filename=Author.pdf");
        OutputStream os = response.getOutputStream();
        byteArrayOutputStream.writeTo(os);
        os.flush();
        os.close();
    }

    @PostMapping("/movie/pdf")
    public void downloadMovieTableToPdf(HttpServletResponse response) throws IOException, DocumentException {
        ByteArrayOutputStream byteArrayOutputStream = pdfGenerator.generatePdfMovies();
        response.setHeader("Content-Disposition", "attachment;filename=Movies.pdf");
        OutputStream os = response.getOutputStream();
        byteArrayOutputStream.writeTo(os);
        os.flush();
        os.close();
    }
}

package com.geekhub.movieblog.download;

import com.geekhub.movieblog.author.Author;
import com.geekhub.movieblog.author.AuthorService;
import com.geekhub.movieblog.movie.Movie;
import com.geekhub.movieblog.movie.MovieService;
import com.geekhub.movieblog.movie.comment.CommentService;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class WordGenerator {

    private final MovieService movieService;
    private final AuthorService authorService;
    private final CommentService commentService;

    @Autowired
    public WordGenerator(MovieService movieService, AuthorService authorService, CommentService commentService) {
        this.movieService = movieService;
        this.authorService = authorService;
        this.commentService = commentService;
    }

    public XWPFDocument generateWordAuthors() {
        XWPFDocument document = new XWPFDocument();

        XWPFTable tableOne = document.createTable();
        XWPFTableRow tableOneRowOne = tableOne.getRow(0);
        tableOneRowOne.getCell(0).setText("Id");
        tableOneRowOne.addNewTableCell().setText("Name");
        tableOneRowOne.addNewTableCell().setText("Gender");
        tableOneRowOne.addNewTableCell().setText("Movie count");

        List<Author> authors = authorService.getAllAuthors();
        Map<Integer, Integer> movieCount = authorService.getMovieCountByAuthorId();

        for (Author author : authors) {
            XWPFTableRow tableOneRowTwo = tableOne.createRow();
            tableOneRowTwo.getCell(0).setText(author.getId().toString());
            tableOneRowTwo.getCell(1).setText(author.getName());
            tableOneRowTwo.getCell(2).setText(String.valueOf(author.getGender()));
            tableOneRowTwo.getCell(3).setText(movieCount.getOrDefault(author.getId(), 0).toString());
        }

        return document;
    }

    public XWPFDocument generateWordMovies() {
        XWPFDocument document = new XWPFDocument();

        XWPFTable tableOne = document.createTable();
        XWPFTableRow tableOneRowOne = tableOne.getRow(0);
        tableOneRowOne.getCell(0).setText("Id");
        tableOneRowOne.addNewTableCell().setText("Name");
        tableOneRowOne.addNewTableCell().setText("Year");
        tableOneRowOne.addNewTableCell().setText("Genre");
        tableOneRowOne.addNewTableCell().setText("Language");
        tableOneRowOne.addNewTableCell().setText("Comment count");
        tableOneRowOne.addNewTableCell().setText("Average rating");

        List<Movie> movies = movieService.getAllMovies();

        for (Movie movie : movies) {
            XWPFTableRow tableOneRowTwo = tableOne.createRow();
            tableOneRowTwo.getCell(0).setText(movie.getId().toString());
            tableOneRowTwo.getCell(1).setText(movie.getName());
            tableOneRowTwo.getCell(2).setText(String.valueOf(movie.getYear()));
            tableOneRowTwo.getCell(3).setText(String.valueOf(movie.getGenre()));
            tableOneRowTwo.getCell(4).setText(String.valueOf(movie.getLanguage()));
            tableOneRowTwo.getCell(5).setText(commentService.getCommentsCount(movie.getId()).toString());
            tableOneRowTwo.getCell(6).setText(commentService.getAverageRating(movie.getId()).toString());
        }

        return document;
    }
}

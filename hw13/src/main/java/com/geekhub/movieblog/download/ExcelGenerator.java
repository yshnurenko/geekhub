package com.geekhub.movieblog.download;

import com.geekhub.movieblog.author.Author;
import com.geekhub.movieblog.author.AuthorService;
import com.geekhub.movieblog.movie.Movie;
import com.geekhub.movieblog.movie.MovieService;
import com.geekhub.movieblog.movie.comment.CommentService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ExcelGenerator {

    private final MovieService movieService;
    private final AuthorService authorService;
    private final CommentService commentService;

    @Autowired
    public ExcelGenerator(MovieService movieService, AuthorService authorService, CommentService commentService) {
        this.movieService = movieService;
        this.authorService = authorService;
        this.commentService = commentService;
    }

    public Workbook generateExcelAuthors() {
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Authors");

        Row header = sheet.createRow(0);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Id");

        headerCell = header.createCell(1);
        headerCell.setCellValue("Name");

        headerCell = header.createCell(2);
        headerCell.setCellValue("Gender");

        headerCell = header.createCell(3);
        headerCell.setCellValue("Movie count");

        List<Author> authors = authorService.getAllAuthors();
        Map<Integer, Integer> movieCount = authorService.getMovieCountByAuthorId();

        for (Author author : authors) {
            Row row = sheet.createRow(author.getId());
            Cell cell = row.createCell(0);
            cell.setCellValue(author.getId());

            cell = row.createCell(1);
            cell.setCellValue(author.getName());

            cell = row.createCell(2);
            cell.setCellValue(String.valueOf(author.getGender()));

            cell = row.createCell(3);
            cell.setCellValue(movieCount.getOrDefault(author.getId(), 0));
        }

        return workbook;
    }

    public Workbook generateExcelMovies() {
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Movies");

        Row header = sheet.createRow(0);
        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Id");

        headerCell = header.createCell(1);
        headerCell.setCellValue("Name");

        headerCell = header.createCell(2);
        headerCell.setCellValue("Year");

        headerCell = header.createCell(3);
        headerCell.setCellValue("Genre");

        headerCell = header.createCell(4);
        headerCell.setCellValue("Language");

        headerCell = header.createCell(5);
        headerCell.setCellValue("Comment count");

        headerCell = header.createCell(6);
        headerCell.setCellValue("Average rating");

        List<Movie> movies = movieService.getAllMovies();

        for (Movie movie : movies) {
            Row row = sheet.createRow(movie.getId());
            Cell cell = row.createCell(0);
            cell.setCellValue(movie.getId());

            cell = row.createCell(1);
            cell.setCellValue(movie.getName());

            cell = row.createCell(2);
            cell.setCellValue(movie.getYear());

            cell = row.createCell(3);
            cell.setCellValue(String.valueOf(movie.getGenre()));

            cell = row.createCell(4);
            cell.setCellValue(String.valueOf(movie.getLanguage()));

            cell = row.createCell(5);
            cell.setCellValue(commentService.getCommentsCount(movie.getId()));

            cell = row.createCell(6);
            cell.setCellValue(commentService.getAverageRating(movie.getId()));
        }

        return workbook;
    }
}

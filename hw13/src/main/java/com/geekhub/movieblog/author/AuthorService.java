package com.geekhub.movieblog.author;

import com.geekhub.movieblog.authormovie.AuthorMovieRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMovieRelationRepository authorMovieRelationRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository, AuthorMovieRelationRepository authorMovieRelationRepository) {
        this.authorRepository = authorRepository;
        this.authorMovieRelationRepository = authorMovieRelationRepository;
    }

    public Author createAuthor(String name, Gender gender) {
        Author author = new Author();
        author.setName(name);
        author.setGender(gender);

        return authorRepository.saveAuthor(author);
    }

    public List<Author> getAllAuthors() {
        return authorRepository.getAllAuthors();
    }

    public Map<Integer, Integer> getMovieCountByAuthorId() {
        return authorMovieRelationRepository.getMovieCountByAuthorId();
    }

    public List<Author> getAuthorById(List<Integer> authorIds) {
        if (authorIds.isEmpty()) {
            return Collections.emptyList();
        }
        return authorRepository.getAuthorsByIds(authorIds);
    }

    public Author getAuthorById(int authorId) {
        return authorRepository.getAuthorById(authorId);
    }
}

package com.geekhub.movieblog.author;

import com.geekhub.movieblog.authormovie.AuthorMovieRelation;
import com.geekhub.movieblog.authormovie.AuthorMovieRelationService;
import com.geekhub.movieblog.movie.Movie;
import com.geekhub.movieblog.movie.MovieDto;
import com.geekhub.movieblog.movie.MovieDtoConverter;
import com.geekhub.movieblog.movie.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/authors")
public class AuthorController {

    private final AuthorService authorService;
    private final AuthorMovieRelationService authorMovieRelationService;
    private final MovieService movieService;
    private final AuthorDtoConverter authorDtoConverter;
    private final MovieDtoConverter movieDtoConverter;

    @Autowired
    public AuthorController(AuthorService authorService,
                            AuthorMovieRelationService authorMovieRelationService,
                            MovieService movieService,
                            AuthorDtoConverter authorDtoConverter,
                            MovieDtoConverter movieDtoConverter) {
        this.authorService = authorService;
        this.authorMovieRelationService = authorMovieRelationService;
        this.movieService = movieService;
        this.authorDtoConverter = authorDtoConverter;
        this.movieDtoConverter = movieDtoConverter;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        Map<Integer, Integer> movieCount = authorService.getMovieCountByAuthorId();

        Set<Integer> authorIds = movieCount.keySet();
        List<Author> authors = authorService.getAuthorById(new ArrayList<>(authorIds));

        List<AuthorWithMovieCountDto> authorsWithMovieCountDto = new ArrayList<>();
        for (Author author : authors) {
            AuthorWithMovieCountDto authorWithMovieCountDto = authorDtoConverter.convert(author, movieCount.get(author.getId()));
            authorsWithMovieCountDto.add(authorWithMovieCountDto);
        }
        model.addAttribute("authors", authorsWithMovieCountDto);

        return "author/index";
    }

    @GetMapping("/create")
    public String getCreatePage(ModelMap model) {
        model.addAttribute("genders", GenderDto.values());
        return "author/create";
    }

    @PostMapping("/create")
    public String createAuthor(@RequestParam String name,
                               @RequestParam GenderDto gender) {

        Author author = authorService.createAuthor(name, Gender.valueOf(gender.name()));
        return "redirect:/authors/" + author.getId() + "/show";
    }

    @GetMapping("/{authorId}/show")
    public String getShowAuthorPage(@PathVariable int authorId, ModelMap model) {
        Author author = authorService.getAuthorById(authorId);
        AuthorDto authorDto = authorDtoConverter.convert(author);
        model.addAttribute("author", authorDto);

        List<AuthorMovieRelation> authorMovieRelations = authorMovieRelationService.getAuthorMovieRelationByAuthorId(authorId);

        List<Integer> movieIds = authorMovieRelations.stream()
                .map(AuthorMovieRelation::getMovieId)
                .collect(Collectors.toList());

        List<Movie> movies = movieService.getMoviesByIds(movieIds);

        List<MovieDto> moviesDto = movies.stream()
                .map(movieDtoConverter::convert)
                .collect(Collectors.toList());

        model.addAttribute("movies", moviesDto);

        return "author/show";
    }
}

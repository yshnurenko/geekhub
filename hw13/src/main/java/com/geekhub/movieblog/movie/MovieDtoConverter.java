package com.geekhub.movieblog.movie;

import org.springframework.stereotype.Component;

@Component
public class MovieDtoConverter {

    public MovieDto convert(Movie movie) {
        MovieDto movieDto = new MovieDto();
        movieDto.setId(movie.getId());
        movieDto.setName(movie.getName());
        movieDto.setYear(movie.getYear());
        movieDto.setCommentCount(movie.getCommentCount());
        movieDto.setAverageRating(movie.getAverageRating());

        GenreDto convertedGenre = GenreDto.valueOf(movie.getGenre().name());
        movieDto.setGenre(convertedGenre);

        LanguageDto convertedLanguage = LanguageDto.valueOf(movie.getLanguage().name());
        movieDto.setLanguage(convertedLanguage);

        return movieDto;
    }
}

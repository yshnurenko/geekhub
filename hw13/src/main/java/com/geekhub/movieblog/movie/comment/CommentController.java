package com.geekhub.movieblog.movie.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/comments")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/create")
    public String createMovie(@RequestParam String name,
                              @RequestParam String massage,
                              @RequestParam int rating,
                              @RequestParam int movieId) {

        commentService.createComment(name, massage, rating, movieId);
        return "redirect:/movies/" + movieId + "/show";
    }
}

package com.geekhub.movieblog.movie.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CommentRowMapper commentRowMapper;

    @Autowired
    public CommentRepository(NamedParameterJdbcTemplate jdbcTemplate, CommentRowMapper commentRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.commentRowMapper = commentRowMapper;
    }

    public void saveComment(Comment comment) {
        String sql = "INSERT INTO comment (name, massage, rating, movie_id) VALUES (:name, :massage, :rating, :movie_id) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", comment.getName());
        params.addValue("massage", comment.getMassage());
        params.addValue("rating", comment.getRating());
        params.addValue("movie_id", comment.getMovieId());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        comment.setId(generatedId);
    }

    public List<Comment> getComments(int movieId) {
        String sql = "SELECT id, name, massage, rating, movie_id FROM comment WHERE movie_id = :movie_id ORDER BY id DESC";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movie_id", movieId);

        return jdbcTemplate.query(sql, params, commentRowMapper);
    }

    public Integer getCommentCount(Integer movieId) {
        String sql = "SELECT COUNT(id) FROM comment WHERE movie_id = :movie_id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movie_id", movieId);

        return jdbcTemplate.queryForObject(sql, params, Integer.class);
    }

    public Integer getAverageRating(Integer movieId) {
        String sql = "SELECT AVG(rating) FROM comment WHERE movie_id = :movie_id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movie_id", movieId);

        Integer averageRating = jdbcTemplate.queryForObject(sql, params, Integer.class);
        if (averageRating == null) {
            return 0;
        }

        return averageRating;
    }
}

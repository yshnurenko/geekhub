package com.geekhub.movieblog.movie;

public enum Genre {
    COMEDY, ACTION, PARODY
}

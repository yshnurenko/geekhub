package com.geekhub.movieblog.movie.comment;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CommentRowMapper implements RowMapper<Comment> {

    @Override
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
        Comment comment = new Comment();
        comment.setId(rs.getInt("id"));
        comment.setName(rs.getString("name"));
        comment.setMassage(rs.getString("massage"));
        comment.setRating(rs.getInt("rating"));
        comment.setMovieId(rs.getInt("movie_id"));

        return comment;
    }
}

package com.geekhub.movieblog.movie.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public List<Comment> getAllComments(int movieId) {
        return commentRepository.getComments(movieId);
    }

    public void createComment(String name, String massage, int rating, int movieId) {
        Comment comment = new Comment();
        comment.setName(name);
        comment.setMassage(massage);
        comment.setRating(rating);
        comment.setMovieId(movieId);

        commentRepository.saveComment(comment);
    }

    public Integer getCommentsCount(Integer movieId) {
        return commentRepository.getCommentCount(movieId);
    }

    public Integer getAverageRating(Integer movieId) {
       return commentRepository.getAverageRating(movieId);
    }
}

package com.geekhub.movieblog.movie;

import com.geekhub.movieblog.author.Author;
import com.geekhub.movieblog.author.AuthorDto;
import com.geekhub.movieblog.author.AuthorDtoConverter;
import com.geekhub.movieblog.author.AuthorService;
import com.geekhub.movieblog.authormovie.AuthorMovieRelation;
import com.geekhub.movieblog.authormovie.AuthorMovieRelationService;
import com.geekhub.movieblog.movie.comment.Comment;
import com.geekhub.movieblog.movie.comment.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/movies")
public class MovieController {

    private final MovieService movieService;
    private final AuthorService authorService;
    private final CommentService commentService;
    private final AuthorMovieRelationService authorMovieRelationService;

    private final MovieDtoConverter movieDtoConverter;
    private final AuthorDtoConverter authorDtoConverter;

    @Autowired
    public MovieController(MovieService movieService,
                           AuthorService authorService,
                           CommentService commentService, AuthorMovieRelationService authorMovieRelationService,
                           MovieDtoConverter movieDtoConverter,
                           AuthorDtoConverter authorDtoConverter) {
        this.movieService = movieService;
        this.authorService = authorService;
        this.commentService = commentService;
        this.authorMovieRelationService = authorMovieRelationService;
        this.movieDtoConverter = movieDtoConverter;
        this.authorDtoConverter = authorDtoConverter;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        List<Movie> movies = movieService.getAllMovies();

        List<MovieDto> moviesDto = movies.stream()
                .map(movieDtoConverter::convert)
                .collect(Collectors.toList());

        for (MovieDto movieDto : moviesDto) {
            movieDto.setCommentCount(commentService.getCommentsCount(movieDto.getId()));
            movieDto.setAverageRating(commentService.getAverageRating(movieDto.getId()));
        }

        model.addAttribute("movies", moviesDto);

        return "movie/index";
    }

    @GetMapping("/create")
    public String getCreatePage(ModelMap model) {
        model.addAttribute("genres", GenreDto.values());
        model.addAttribute("languages", LanguageDto.values());

        List<Author> authors = authorService.getAllAuthors();

        List<AuthorDto> authorsDto = authors.stream()
                .map(authorDtoConverter::convert)
                .collect(Collectors.toList());

        model.addAttribute("authors", authorsDto);

        return "movie/create";
    }

    @PostMapping("/create")
    public String createMovie(@RequestParam String name,
                              @RequestParam int year,
                              @RequestParam GenreDto genre,
                              @RequestParam LanguageDto language,
                              @RequestParam List<Integer> authorIds) {

        Movie movie = movieService.createMovie(name, year, Genre.valueOf(genre.name()), Language.valueOf(language.name()), authorIds);
        return "redirect:/movies/" + movie.getId() + "/show";
    }

    @GetMapping("/{movieId}/show")
    public String getShowMoviePage(@PathVariable int movieId, ModelMap model) {
        Movie movie = movieService.getMovieById(movieId);
        model.addAttribute("movie", movieDtoConverter.convert(movie));

        List<Comment> comments = commentService.getAllComments(movieId);
        model.addAttribute("comments", comments);

        List<AuthorMovieRelation> authorMovieRelations = authorMovieRelationService.getAuthorMovieRelationByMovieId(movieId);

        List<Integer> authorIds = authorMovieRelations.stream()
                .map(AuthorMovieRelation::getAuthorId)
                .collect(Collectors.toList());

        List<Author> authors = authorService.getAuthorById(authorIds);

        List<AuthorDto> authorsDto = authors.stream()
                .map(authorDtoConverter::convert)
                .collect(Collectors.toList());

        model.addAttribute("authors", authorsDto);

        return "movie/show";
    }
}

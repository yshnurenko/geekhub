package com.geekhub.movieblog.movie;

public enum Language {
    ENGLISH, SPANISH, UKRAINIAN
}

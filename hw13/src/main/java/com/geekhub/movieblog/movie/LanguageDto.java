package com.geekhub.movieblog.movie;

public enum LanguageDto {
    ENGLISH, SPANISH, UKRAINIAN
}

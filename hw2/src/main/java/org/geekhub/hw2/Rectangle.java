package org.geekhub.hw2;

import java.util.LinkedHashMap;
import java.util.Map;

public class Rectangle implements Shape {
    private double a;
    private double b;

    Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculateArea() {
        return a * b;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (a + b);
    }

    Map<String, Double> separation() {
        Map<String, Double> linkedHashMap = new LinkedHashMap<>();

        double diagonal = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        Triangle firstTriangle = new Triangle(a, b, diagonal);
        double areaFirstTriangle = firstTriangle.calculateArea();
        double perimeterFirstTriangle = firstTriangle.calculatePerimeter();

        linkedHashMap.put("First triangle area", areaFirstTriangle);
        linkedHashMap.put("First triangle perimeter", perimeterFirstTriangle);

        Triangle secondTriangle = new Triangle(a, b, diagonal);
        double areaSecondTriangle = secondTriangle.calculateArea();
        double perimeterSecondTriangle = secondTriangle.calculatePerimeter();

        linkedHashMap.put("Second triangle area", areaSecondTriangle);
        linkedHashMap.put("Second triangle perimeter", perimeterSecondTriangle);
        return linkedHashMap;
    }
}
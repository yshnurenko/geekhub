package org.geekhub.hw2;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the name of the figure: ");
        String input = scanner.nextLine();
        ShapeType shapeType = ShapeType.valueOf(input);

        switch (shapeType) {
            case CIRCLE:
                System.out.println("Enter circle radius: ");
                Circle circle = new Circle(scanner.nextDouble());
                System.out.println("Area: " + circle.calculateArea());
                System.out.println("Perimeter: " + circle.calculatePerimeter());
                break;
            case TRIANGLE:
                System.out.println("Enter the value of the sides triangle: ");
                String sideTriangle = scanner.nextLine();
                String[] arraySidesTriangle = sideTriangle.split(",");
                double a = Double.parseDouble(arraySidesTriangle[0]);
                double b = Double.parseDouble(arraySidesTriangle[1]);
                double c = Double.parseDouble(arraySidesTriangle[2]);
                Triangle triangle = new Triangle(a, b, c);
                System.out.println("Area: " + triangle.calculateArea());
                System.out.println("Perimeter: " + triangle.calculatePerimeter());
                break;
            case SQUARE:
                System.out.println("Enter the side of the square: ");
                Square square = new Square(scanner.nextDouble());
                System.out.println("Area: " + square.calculateArea());
                System.out.println("Perimeter: " + square.calculatePerimeter());
                System.out.println("Consists of two triangles:" + square.separation().toString());
                break;
            case RECTANGLE:
                System.out.println("Enter the sides of the rectangle: ");
                String sideRectangle = scanner.nextLine();
                String[] arraySidesRectangle = sideRectangle.split(",");
                a = Double.parseDouble(arraySidesRectangle[0]);
                b = Double.parseDouble(arraySidesRectangle[1]);
                Rectangle rectangle = new Rectangle(a, b);
                System.out.println("Area: " + rectangle.calculateArea());
                System.out.println("Perimeter: " + rectangle.calculatePerimeter());
                System.out.println("Consists of two triangles:" + rectangle.separation().toString());
                break;
            default:
                System.out.println("You entered the wrong shape name.");
                break;
        }
    }
}
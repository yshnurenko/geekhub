package org.geekhub.hw2;

public enum ShapeType {
    CIRCLE,
    TRIANGLE,
    SQUARE,
    RECTANGLE;
}
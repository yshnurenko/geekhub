package org.geekhub.hw2;

public class Square extends Rectangle implements Shape {
    private double a;

    Square(double a) {
        super(a, a);
        this.a = a;
    }

    @Override
    public double calculateArea() {
        return Math.pow(a, 2);
    }

    @Override
    public double calculatePerimeter() {
        return 4 * a;
    }
}
package org.geekhub.hw2;

public class Circle implements Shape {
    private double radius;
    private final double numberP = 3.14;

    Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return numberP * Math.pow(radius, 2);
    }

    @Override
    public double calculatePerimeter() {
        return 2 * numberP * radius;
    }
}